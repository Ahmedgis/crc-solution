﻿Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.Skins
Imports DevExpress.LookAndFeel
Imports DevExpress.UserSkins
Imports DevExpress.XtraBars
Imports DevExpress.XtraBars.Ribbon
Imports DevExpress.XtraBars.Helpers
Imports DevExpress.XtraGrid
Imports System.IO
Imports System.Text.RegularExpressions
Imports DevExpress.Utils
Imports System.Globalization
Imports System.Linq
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports DevExpress.XtraGrid.Drawing
Imports DevExpress.XtraEditors
Imports DevExpress.XtraSplashScreen

'--------------------------------------------------------------
' Project   : United ATS CRC32 Validator
' Developer : Eng. Ahmed Samy
' E-mail    : Ahmedgis@ymail.com
' Date      : 7/5/2013
' Updated	: 20/10/2019
'--------------------------------------------------------------


Public Class FrmMain

#Region "     Variables"

	Dim _crcFileGeneratorTool As String = String.Empty
	Dim _crcFileType As String = String.Empty
	Dim _crcFileCreationDate As String = String.Empty
	Dim _crcFileDataOriginator As String = String.Empty
	Dim _crcFileCompanyName As String = String.Empty
	Dim _crcFileSourceFileName As String = String.Empty
	Dim _crcFileLastModified As String = String.Empty

	Dim _crcTagFileGeneratorTool As String = "# Generator:"
	Dim _crcTagFileType As String = "# File Type:"
	Dim _crcTagFileCreationDate As String = "# Creation Date:"
	Dim _crcTagFileDataOriginator As String = "# Data Originator:"
	Dim _crcTagFileCompanyName As String = "# Company Name:"
	Dim _crcTagFileSourceFileName As String = "# Source Filename:"
	Dim _crcTagFileLastModified As String = "# Last Modified:"

	Dim _crcFileRecordsSplitFields As List(Of String())
	Dim _crcFileRecordsCommaFields As List(Of String)

	Dim _validCrcRecordsCount As Double = 0

	' The Grid control DataTable
	Dim recordsDataTable As New DataTable

	Public inputFileName As String = ""

	Dim _is1stRowIsHeader As Boolean = False

	Dim STATUS_FEILD_NAME As String = "STATUS"

#End Region

	Sub New()
		InitSkins()
		InitializeComponent()
		Me.InitSkinGallery()
	End Sub
	Sub New(ByVal filename As String)
		MyClass.New()

		Me.inputFileName = filename
	End Sub

	Sub InitSkins()
		DevExpress.Skins.SkinManager.EnableFormSkins()
		DevExpress.UserSkins.BonusSkins.Register()
		UserLookAndFeel.Default.SetSkinStyle("DevExpress Style")

	End Sub
	Private Sub InitSkinGallery()
		SkinHelper.InitSkinGallery(rgbiSkins, True)
	End Sub



#Region "     Load CRC File Fieldsto the grid"

	Private Function ReadCcrFile(ByVal vFileName As String, is1stRowHeader As Boolean, ByRef headerColumns As List(Of String)) As Integer
		' Read CRC File
		' Return Fields Count
		Dim inline As String = String.Empty
		Dim _1stRowSkipped As Boolean = False

		If FileSystem.FileLen(vFileName) <= 0 Then Return -1 ' means no fields found

		If File.Exists(vFileName) Then
			Dim infile As StreamReader = New StreamReader(vFileName)

			If infile.EndOfStream = False Then
				While (infile.EndOfStream = False)

					inline = infile.ReadLine()

					If Not String.IsNullOrEmpty(inline) Then

						'	Check for the wrong line end, remove the last comma - if any -
						If inline.EndsWith(",") Then
							inline = inline.Substring(0, inline.Length - 1)
						End If

						If inline.StartsWith("#") Then
							' Parse File header
							ParseCcrFileHeaderLine(inline)

						Else
							' parse file records, put them in a collection of string array [from Split fn]
							ParseCcrFileRecordLine(inline)

						End If
					End If
				End While


				infile.Close()
				infile.Dispose()

				Return 1

			End If
		Else
			XtraMessageBox.Show("File not found in the selected path" & vbNewLine & vFileName, "Error Opening file", MessageBoxButtons.OK, MessageBoxIcon.Error)
		End If



	End Function

	Private Sub ParseCcrFileHeaderLine(ByVal headerlineStr As String)
		' Parse the CRC File Header Line per Line to extract the CRC file Header information

		If headerlineStr.StartsWith(_crcTagFileGeneratorTool, True, CultureInfo.CreateSpecificCulture("en-us")) Then
			_crcFileGeneratorTool = headerlineStr.Substring(_crcTagFileGeneratorTool.Length, headerlineStr.Length - _crcTagFileGeneratorTool.Length).Trim()
		ElseIf headerlineStr.StartsWith(_crcTagFileType, True, CultureInfo.CreateSpecificCulture("en-us")) Then
			_crcFileType = headerlineStr.Substring(_crcTagFileType.Length, headerlineStr.Length - _crcTagFileType.Length).Trim()
		ElseIf headerlineStr.StartsWith(_crcTagFileCreationDate, True, CultureInfo.CreateSpecificCulture("en-us")) Then
			_crcFileCreationDate = headerlineStr.Substring(_crcTagFileCreationDate.Length, headerlineStr.Length - _crcTagFileCreationDate.Length).Trim()
		ElseIf headerlineStr.StartsWith(_crcTagFileDataOriginator, True, CultureInfo.CreateSpecificCulture("en-us")) Then
			_crcFileDataOriginator = headerlineStr.Substring(_crcTagFileDataOriginator.Length, headerlineStr.Length - _crcTagFileDataOriginator.Length).Trim()
		ElseIf headerlineStr.StartsWith(_crcTagFileCompanyName, True, CultureInfo.CreateSpecificCulture("en-us")) Then
			_crcFileCompanyName = headerlineStr.Substring(_crcTagFileCompanyName.Length, headerlineStr.Length - _crcTagFileCompanyName.Length).Trim()
		ElseIf headerlineStr.StartsWith(_crcTagFileSourceFileName, True, CultureInfo.CreateSpecificCulture("en-us")) Then
			_crcFileSourceFileName = headerlineStr.Substring(_crcTagFileSourceFileName.Length, headerlineStr.Length - _crcTagFileSourceFileName.Length).Trim()
		ElseIf headerlineStr.StartsWith(_crcTagFileLastModified, True, CultureInfo.CreateSpecificCulture("en-us")) Then
			_crcFileLastModified = headerlineStr.Substring(_crcTagFileLastModified.Length, headerlineStr.Length - _crcTagFileLastModified.Length).Trim()
		End If
	End Sub

	Private Function ParseCcrFileHeaderRow(ByVal headerRowStr As String) As List(Of String)
		' Parse the CRC File Header row to extract the CRC file Header columns

		Dim headerColumns() As String

		If Not String.IsNullOrEmpty(headerRowStr) Then
			headerColumns = headerRowStr.Split(",")
		End If

		Return headerColumns.ToList()

	End Function

	Private Sub ParseCcrFileRecordLine(ByVal recordlineStr As String)

		Dim recordFldsValArr() = recordlineStr.Replace("""", "").Split(",")

		If recordFldsValArr.Length > 0 Then

			' Add the Record Fields Value Array to the Collection

			_crcFileRecordsSplitFields.Add(recordFldsValArr)

			_crcFileRecordsCommaFields.Add(recordlineStr.Replace("""", ""))

		End If


	End Sub
	Private Function SelectOnlyCrcFieldValue(ByVal recordlineStrSplitted() As String) As String
		' Take the splitted record string and strip the last array element as the CRC32 field value to be check later
		Try
			Return recordlineStrSplitted(recordlineStrSplitted.Length - 1).ToUpper()
		Catch ex As System.Exception
			Return "00000000"
		End Try
	End Function
	Private Function SelectOnlyCrcFieldValue(ByVal recordlineStrComma As String) As String
		' Take the splitted record string and strip the last array element as the CRC32 field value to be check later
		Try
			Dim lastCommaIndex As Integer = recordlineStrComma.LastIndexOf(",")

			If lastCommaIndex > 0 Then
				Return recordlineStrComma.Substring(lastCommaIndex + 1, recordlineStrComma.Length - lastCommaIndex - 1).ToUpper()
			Else
				Return ""
			End If
		Catch ex As System.Exception
			Return "00000000"
		End Try
	End Function
	Private Function ExcludeCrcFieldValue(ByVal recordlineStrComma As String) As String
		' Remove the splitted record string and return the string without it
		Try
			Dim lastCommaIndex As Integer = recordlineStrComma.LastIndexOf(",")

			If lastCommaIndex > 0 Then
				Return recordlineStrComma.Substring(0, lastCommaIndex)
			Else
				Return ""
			End If
		Catch ex As System.Exception
			Return ""
		End Try


	End Function
	Private Function IsValidCrcFldType(ByVal crcFldValue As String) As Boolean
		' Check if the Last field of the Obstacle or Aerodrome facility is a CRC Field or not

		' Check the CRC Field Value Conditions: [Using Regular expression]
		' 1- Length = 8 Char
		' 2- ASCII Letters (A B C D E F)

		Dim foundMatch As Boolean = Regex.IsMatch(crcFldValue, "[A-Fa-f0-9]{8}", RegexOptions.IgnoreCase)

		If foundMatch Then
			Return True
		Else
			Return False
		End If

	End Function

	Private Function CalculateCRCValue(ByVal recordStr As String) As String
		Dim clsCrc32 As New ClsCRC32Q(recordStr)

		Return clsCrc32.CrcValue.ToUpper()

	End Function

	Private Sub AddFileRecordsToGridAndCheckCrc(ByRef vGridControl As GridControl)

		'Reset Valid CRC Records counter
		_validCrcRecordsCount = 0


		RepositoryItemProgressBar1.Step = 1
		RepositoryItemProgressBar1.PercentView = True
		RepositoryItemProgressBar1.Maximum = _crcFileRecordsSplitFields.Count
		RepositoryItemProgressBar1.Minimum = 0
		crcValidationProgress.EditValue = RepositoryItemProgressBar1.Minimum
		crcValidationProgress.Width = 150

		For Each recordStr As String In _crcFileRecordsCommaFields
			Dim recordStrWithoutCrcFld As String = ExcludeCrcFieldValue(recordStr)

			Dim givenCrcRecordValue As String = SelectOnlyCrcFieldValue(recordStr)

			' Check if the Given CRC Field Value is Valid CRC value or the record didn't ends with a CRC field value
			Dim recordStatusStr As String = "Valid CRC"

			If Not IsValidCrcFldType(givenCrcRecordValue) Then
				'recordStatusStr = "CRC Field not found"
				recordStatusStr = "Invalid CRC"
			Else
				' Calculate the CRC32 value for the comma record string
				Dim calculatedCrcRecordValue As String = CalculateCRCValue(recordStrWithoutCrcFld)

				' Compaire the (Given, Calculated) CRC values
				Dim isValidGivenCrcValue As Boolean = (givenCrcRecordValue = calculatedCrcRecordValue)

				If Not isValidGivenCrcValue Then recordStatusStr = "Invalid CRC"
			End If

			If recordStatusStr = "Valid CRC" Then _validCrcRecordsCount += 1


			' Add the record to the Grid control
			AddRowToGrid(recordStr, recordsDataTable, recordStatusStr)


			crcValidationProgress.EditValue += 1
			Application.DoEvents()

		Next

		' Strip CRC Field and check its validity
	End Sub
	Private Function IsAllUpperCase(ByVal value As String) As Boolean
		Return (value.Equals(value.ToUpper()))
	End Function

	Private Function IsHeaderRow(ByVal rowComma As String) As Boolean
		If Not String.IsNullOrEmpty(rowComma) Then
			Dim rowCommaArr() As String = rowComma.Split(",")

			If rowCommaArr.Length > 0 Then
				For Each fld As String In rowCommaArr
					If IsNumeric(fld) OrElse String.IsNullOrEmpty(fld) Then
						Return False
					End If
				Next

				'	If NO field value is numeric, so this might be a header row
				Return True
			Else
				Return False
			End If
		Else
			Return False
		End If
	End Function


	Private Function GetMaxFldsCount() As Integer
		' Get the max crc file record fields count to build the table acc.to the maximum

		Dim maxFldCount As Integer = 0

		For i = 0 To _crcFileRecordsSplitFields.Count - 1
			If _crcFileRecordsSplitFields(i).Length > maxFldCount Then
				maxFldCount = _crcFileRecordsSplitFields(i).Length
			End If
		Next

		Return maxFldCount
	End Function
	Private Function CreateCustomGridColsHeader(ByVal colFldNamesSplitted() As String, ByVal maxFldsCountr As Integer, ByRef errorMsg As String) As DataTable
		Dim tbl As New DataTable()

		For j As Integer = 0 To colFldNamesSplitted.Length - 1

			Try
				tbl.Columns.Add(colFldNamesSplitted(j), GetType(String))
			Catch ex As Exception
				'	Exception will occure if the column name is existed before
				'	so we add an index to that word
				'Dim wordIndex As String = colFldNamesSplitted(j).Substring(colFldNamesSplitted(j).Length - 1, 1)
				'If IsNumeric(wordIndex) Then
				'	tbl.Columns.Add(String.Format("{0}_{1}", colFldNamesSplitted(j), CInt(wordIndex) + 1), GetType(String))
				'else
				'	tbl.Columns.Add(String.Format("{0}_1", colFldNamesSplitted(j)), GetType(String))
				'End If

				errorMsg = ex.Message

				Return Nothing

			End Try
		Next

		For i As Integer = 0 To maxFldsCountr - colFldNamesSplitted.Length - 1
			If i <= 25 Then         '  A,B,C,D,F, ..., Z
				tbl.Columns.Add(Chr(i + 65), GetType(String))
			ElseIf i > 25 Then      '  A1, A2, A3,... A(n)
				tbl.Columns.Add("A" & i - 25, GetType(String))
			End If

		Next i

		'	check if CRC FIELD name is existed before
		If tbl.Columns.Contains(STATUS_FEILD_NAME) Then

			' Add an extra column for The record's CRC value
			tbl.Columns.Add(String.Format("_{0}_", STATUS_FEILD_NAME), GetType(String))

			'	Update Status field name
			STATUS_FEILD_NAME = String.Format("_{0}_", STATUS_FEILD_NAME)
		Else
			' Add an extra column for The record's CRC value
			tbl.Columns.Add(STATUS_FEILD_NAME, GetType(String))
		End If

		Return tbl
	End Function
	Private Function CreateRandomGridColsHeader(ByVal ColCount As Integer) As DataTable
		Dim tbl As New DataTable()

		For i As Integer = 0 To ColCount - 1
			If i <= 25 Then         '  A,B,C,D,F, ..., Z
				tbl.Columns.Add(Chr(i + 65), GetType(String))
			ElseIf i > 25 Then      '  A1, A2, A3,... A(n)
				tbl.Columns.Add("A" & i - 25, GetType(String))
			End If

		Next i

		'	check if CRC FIELD name is existed before
		If tbl.Columns.Contains(STATUS_FEILD_NAME) Then
			' Add an extra column for The record's CRC value
			tbl.Columns.Add(String.Format("_{0}_", STATUS_FEILD_NAME), GetType(String))

			'	Update Status field name
			STATUS_FEILD_NAME = String.Format("_{0}_", STATUS_FEILD_NAME)
		Else
			' Add an extra column for The record's CRC value
			tbl.Columns.Add(STATUS_FEILD_NAME, GetType(String))
		End If

		Return tbl
	End Function

	Private Sub AddRowToGrid(ByVal rowStrComma As String, ByRef dataTable As DataTable, ByVal recordStatusStr As String)

		If rowStrComma.Length > 0 Then rowStrComma += "," & recordStatusStr

		Dim rowStrFldsValArr() = rowStrComma.Split(",")

		'If rowStrFldsValArr.Length < recordsDataTable.Columns.Count Then     ' the record fiels is less than expected, so fill the missing fields with blanks
		'    Dim missingFldsCount As Integer = recordsDataTable.Columns.Count - rowStrFldsValArr.Length
		'End If

		If rowStrFldsValArr.Length > 0 Then
			dataTable.Rows.Add(rowStrFldsValArr)
		End If

	End Sub

	'Private Function AddRowToGrid(ByVal RowStrArr() As String) As DataTable
	'    'Dim tbl As New DataTable()
	'    'tbl.Columns.Add("Name", GetType(String))
	'    'tbl.Columns.Add("ID", GetType(Integer))
	'    'tbl.Columns.Add("Number", GetType(Integer))
	'    'tbl.Columns.Add("Date", GetType(Date))
	'    'For i As Integer = 0 To RowCount - 1
	'    '    tbl.Rows.Add(New Object() {String.Format("Name{0}", i), i, 3 - i, Date.Now.AddDays(i)})
	'    'Next i
	'    'Return tbl
	'End Function

	Private Sub CreateColumns(ByVal gc As GridControl)
		'' Create Grid columns.
		'gc.BeginUpdate()
		'gc.Columns.Add()
		'gc.Columns(0).Caption = "Customer"
		'gc.Columns(0).VisibleIndex = 0
		'gc.Columns.Add()
		'gc.Columns(1).Caption = "Location"
		'gc.Columns(1).VisibleIndex = 1
		'gc.Columns.Add()
		'gc.Columns(2).Caption = "Phone"
		'gc.Columns(2).VisibleIndex = 2
		'gc.EndUpdate()
	End Sub

	Private Sub CreateNodes(ByVal gc As GridControl)
		'gc.BeginUnboundLoad()
		'' Create a root node .
		'Dim parentForRootNodes As TreeListNode = Nothing
		'Dim rootNode As TreeListNode = gc.AppendNode(New Object() {"Alfreds Futterkiste", "Germany, Obere Str. 57", "030-0074321"}, parentForRootNodes)
		'' Create a child node for the node1            
		'gc.AppendNode(New Object() {"Suyama, Michael", "Obere Str. 55", "030-0074263"}, rootNode)
		'' Creating more nodes
		'' ...
		'gc.EndUnboundLoad()
	End Sub
#End Region


#Region "       Read File       "
	Public Function ReadFile(ByVal fileName As String) As String

		Dim headerColumns As New List(Of String)

		If Not String.IsNullOrEmpty(fileName) Then
			'----------------------------------------------------
			' Read the CRC file and parse all its records'----------------------------------------------------
			_crcFileRecordsSplitFields = New List(Of String())()
			_crcFileRecordsCommaFields = New List(Of String)()

			Call ReadCcrFile(fileName, False, headerColumns)

			'recordsDataTable = CreateGridColsHeader(_crcFileRecordsSplitFields(0).Length)

			'------------------------------------------------------------------------------------
			' Check the 1st Line of the file if it's the Table header or it's a record value
			' we can check this with many ways:
			'--------------------------------------
			' 1- check if the record contain any spaces, that not allowed in the crc file record. [_crcFileRecordsCommaFields(0).Contains(" ") Or _]
			' 2- check if the 1st field in the record (SiteName) more than 4 characters.
			' 3- check if all fields must be in Upper Case
			'------------------------------------------------------------------------------------
			Dim errorMsg As String = ""

			'If _crcFileRecordsCommaFields(0).Split(",")(0).Length > 4 OrElse IsAllUpperCase(_crcFileRecordsCommaFields(0)) = False Then
			If IsHeaderRow(_crcFileRecordsCommaFields(0)) Then

				'the record is the table header
				recordsDataTable = CreateCustomGridColsHeader(_crcFileRecordsSplitFields(0), GetMaxFldsCount, errorMsg)

				' Reomve the 1st item in the array that hold the crc file records
				' to avoid adding the header record again as a normal crc file record

				_crcFileRecordsSplitFields.RemoveAt(0)
				_crcFileRecordsCommaFields.RemoveAt(0)
			Else
				recordsDataTable = CreateRandomGridColsHeader(GetMaxFldsCount)
			End If
			'----------------------------------------------------

			'	Check if an error happend while parsing the file header columns
			If recordsDataTable Is Nothing And Not String.IsNullOrEmpty(errorMsg) Then
				Return errorMsg
			End If


			'----------------------------------------------------
			' Show CRC File Header Information
			'----------------------------------------------------
			'TxtGeneratorTool.Text = _crcFileGeneratorTool
			'TxtFileType.Text = _crcFileType
			'TxtCreationDate.Text = _crcFileCreationDate
			'TxtDataOriginator.Text = _crcFileDataOriginator
			'TxtCompanyName.Text = _crcFileCompanyName
			'TxtSourceFileName.Text = _crcFileSourceFileName
			'TxtLastModified.Text = _crcFileLastModified
			'----------------------------------------------------

			Application.DoEvents()

			'---------------------------------------------------------------------------
			' Add all collected records to the Grid Control and check the crc values
			'---------------------------------------------------------------------------
			Call AddFileRecordsToGridAndCheckCrc(gridControl)

			'----------------------------------------------------
			' Display File details
			'----------------------------------------------------
			siStatus.Caption = "CRC File Details: "
			siCrcTotalRecords.Caption = "Total Records: " & recordsDataTable.Rows.Count
			siCrcValidRecords.Caption = "Valid CRC Records: " & _validCrcRecordsCount
			siCrcInvalidRecords.Caption = "Invalid CRC Records: " & recordsDataTable.Rows.Count - _validCrcRecordsCount
			'----------------------------------------------------


			AlertControl1.Show(Me, Application.ProductName, "File CRC validation complete" & vbNewLine & recordsDataTable.Rows.Count & "  total Records" & vbNewLine & _validCrcRecordsCount & "  records Valid CRC" & vbNewLine & recordsDataTable.Rows.Count - _validCrcRecordsCount & "  records inValid CRC")

			'ToolTipController1.ShowHint("aaahgh", siCrcValidRecords, ToolTipLocation.Default)
			'ToolTipController.DefaultController.ShowHint("aaa", "aaahgh", siCrcValidRecords, ToolTipLocation.Default, PointToScreen((New Point(0, 0))))

			gridControl.DataSource = recordsDataTable
			gridControl.RefreshDataSource()

			Application.DoEvents()

			gridView1.BestFitColumns()
			gridView1.OptionsSelection.EnableAppearanceFocusedRow = True
			gridView1.OptionsSelection.EnableAppearanceHideSelection = False
			'gridView1.OptionsView.ColumnAutoWidth = True
			gridView1.OptionsView.ShowGroupPanel = False
			gridView1.OptionsNavigation.AutoFocusNewRow = False

			gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = HorizontalAlignment.Center

			gridView1.OptionsBehavior.ReadOnly = True
			gridView1.OptionsBehavior.Editable = False

			'----------------------------------------------------
			iOpen.Enabled = False
			iClose.Enabled = True
			iReOpen.Enabled = True

			Return ""
		ElseIf Not File.Exists(fileName) Then
			Return String.Format("The selected file: {0}{1}{1}Does not exist, please select ( Open ) File and try again", fileName, vbNewLine)
		Else
			Return String.Format("Unknown Error While Opening file:{1}{0}", fileName, vbNewLine)
		End If
	End Function
#End Region

	Private Sub gridView1_RowClick(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.RowClickEventArgs) Handles gridView1.RowClick
		ToolTipController1.ShowHint(gridView1.GetRowCellDisplayText(e.HitInfo.RowHandle, STATUS_FEILD_NAME))
	End Sub


	Private Sub gridView1_RowStyle(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles gridView1.RowStyle
		If e.RowHandle > -1 Then
			'Try
			If gridView1.GetRowCellDisplayText(e.RowHandle, STATUS_FEILD_NAME).ToLower() = "valid crc" Then
				e.Appearance.BackColor = Color.LightGreen
			Else
				e.Appearance.BackColor = Color.Red
			End If
			'Catch ex As System.Exception
			'    XtraMessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
			'End Try

			e.Appearance.TextOptions.HAlignment = HorizontalAlignment.Center

		End If
	End Sub

	Private Sub iExit_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles iExit.ItemClick
		Dim vDialogResult As DialogResult = MessageBox.Show("Exit the program?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

		If vDialogResult = DialogResult.Yes Then
			Application.Exit()
		End If
	End Sub

	Private Sub iOpen_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles iOpen.ItemClick
		'OpenFileDialog1.Filter = "CRC Files (*.CRC)|*.crc|Comma Delimited files (*.*)|*.*"
		OpenFileDialog1.Filter = "CRC Files (*.CRC)|*.crc"
		OpenFileDialog1.Title = "Select file to open"
		OpenFileDialog1.CheckFileExists = True
		OpenFileDialog1.AddExtension = True
		OpenFileDialog1.FileName = ""

		Dim vRes As DialogResult = OpenFileDialog1.ShowDialog()
		If vRes = DialogResult.Cancel Then Exit Sub

		'   Read the selected file

		Me.inputFileName = OpenFileDialog1.FileName

		'	Show wait message
		Call ShowWaitFormOnTheButtomCenter("Reading CRC file", "Please wait ...")

		Dim result As String = ""

		Try
			result = Me.ReadFile(Me.inputFileName)

		Catch ex As Exception
			AlertControl1.Show(Me, "Reading CRC file", "Invalid CRC file")
			XtraMessageBox.Show(ex.Message, "Error Reading CRC file", MessageBoxButtons.OK, MessageBoxIcon.Error)
		End Try

		Try

			'	Show wait message
			SplashScreenManager.CloseDefaultWaitForm()

		Catch ex As Exception

		End Try

		If Not String.IsNullOrEmpty(result) Then
			AlertControl1.Show(Me, Application.ProductName, result)
		End If

#Region "		Unused Code		"

		'      If Not String.IsNullOrEmpty(OpenFileDialog1.FileName) Then
		'	'----------------------------------------------------
		'	' Read the CRC file and parse all its records
		'	'----------------------------------------------------
		'	_crcFileRecordsSplitFields = New List(Of String())()
		'	_crcFileRecordsCommaFields = New List(Of String)()

		'	ReadCcrFile(OpenFileDialog1.FileName)

		'	'recordsDataTable = CreateGridColsHeader(_crcFileRecordsSplitFields(0).Length)

		'	'------------------------------------------------------------------------------------
		'	' Check the 1st Line of the file if it's the Table header or it's a record value
		'	' we can check this with many ways:
		'	'--------------------------------------
		'	' 1- check if the record contain any spaces, that not allowed in the crc file record. [_crcFileRecordsCommaFields(0).Contains(" ") Or _]
		'	' 2- check if the 1st field in the record (SiteName) more than 4 characters.
		'	' 3- check if all fields must be in Upper Case
		'	'------------------------------------------------------------------------------------

		'	If _crcFileRecordsCommaFields(0).Split(",")(0).Length > 4 Or
		'	   IsAllUpperCase(_crcFileRecordsCommaFields(0)) = False Then

		'		'the record is the table header
		'		recordsDataTable = CreateCustomGridColsHeader(_crcFileRecordsSplitFields(0), GetMaxFldsCount)

		'		' Reomve the 1st item in the array that hold the crc file records
		'		' to avoid adding the header record again as a normal crc file record

		'		_crcFileRecordsSplitFields.RemoveAt(0)
		'		_crcFileRecordsCommaFields.RemoveAt(0)
		'	Else
		'		recordsDataTable = CreateRandomGridColsHeader(GetMaxFldsCount)
		'	End If
		'	'----------------------------------------------------


		'	'----------------------------------------------------
		'	' Show CRC File Header Information
		'	'----------------------------------------------------
		'	TxtGeneratorTool.Text = _crcFileGeneratorTool
		'	TxtFileType.Text = _crcFileType
		'	TxtCreationDate.Text = _crcFileCreationDate
		'	TxtDataOriginator.Text = _crcFileDataOriginator
		'	TxtCompanyName.Text = _crcFileCompanyName
		'	TxtSourceFileName.Text = _crcFileSourceFileName
		'	TxtLastModified.Text = _crcFileLastModified
		'	'----------------------------------------------------

		'	Application.DoEvents()

		'	'---------------------------------------------------------------------------
		'	' Add all collected records to the Grid Control and check the crc values
		'	'---------------------------------------------------------------------------
		'	AddFileRecordsToGridAndCheckCrc(gridControl)

		'	'----------------------------------------------------
		'	' Display File details
		'	'----------------------------------------------------
		'	siStatus.Caption = "CRC File Details: "
		'	siCrcTotalRecords.Caption = "Total Records: " & recordsDataTable.Rows.Count
		'	siCrcValidRecords.Caption = "Valid CRC Records: " & _validCrcRecordsCount
		'	siCrcInvalidRecords.Caption = "Invalid CRC Records: " & recordsDataTable.Rows.Count - _validCrcRecordsCount
		'	'----------------------------------------------------


		'	AlertControl1.Show(Me, Application.ProductName, "File CRC validation complete" & vbNewLine & recordsDataTable.Rows.Count & "  total Records" & vbNewLine & _validCrcRecordsCount & "  records Valid CRC" & vbNewLine & recordsDataTable.Rows.Count - _validCrcRecordsCount & "  records inValid CRC")

		'	'ToolTipController1.ShowHint("aaahgh", siCrcValidRecords, ToolTipLocation.Default)
		'	'ToolTipController.DefaultController.ShowHint("aaa", "aaahgh", siCrcValidRecords, ToolTipLocation.Default, PointToScreen((New Point(0, 0))))

		'	gridControl.DataSource = recordsDataTable
		'	gridControl.RefreshDataSource()

		'	Application.DoEvents()

		'	gridView1.BestFitColumns()
		'	gridView1.OptionsSelection.EnableAppearanceFocusedRow = True
		'	gridView1.OptionsSelection.EnableAppearanceHideSelection = False
		'	'gridView1.OptionsView.ColumnAutoWidth = True
		'	gridView1.OptionsView.ShowGroupPanel = False
		'	gridView1.OptionsNavigation.AutoFocusNewRow = False

		'	gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = HorizontalAlignment.Center

		'	gridView1.OptionsBehavior.ReadOnly = True
		'	gridView1.OptionsBehavior.Editable = False

		'	'----------------------------------------------------
		'	iOpen.Enabled = False
		'	iClose.Enabled = True

		'	'---------------------------------------------------------------------------
		'	' Todo: Adjust Grid Columns width not with the buit-in properties
		'	'----------------------------------------------------
		'	' Adjust GridControl Hozrizontal extension
		'	'----------------------------------------------------
		'	'' ''gridControl.ForceInitialize()
		'	'' ''Dim vi As GridViewInfo = TryCast(gridView1.GetViewInfo(), GridViewInfo)
		'	'' ''Dim sizes As New Dictionary(Of GridColumn, Integer)()
		'	'' ''For Each info As GridColumnInfoArgs In vi.ColumnsInfo
		'	'' ''    If info.Column Is Nothing Then
		'	'' ''        Continue For
		'	'' ''    End If
		'	'' ''    sizes.Add(info.Column, info.Bounds.Width)
		'	'' ''Next info
		'	'' ''gridView1.OptionsView.ColumnAutoWidth = False
		'	'' ''For Each c As GridColumn In gridView1.Columns
		'	'' ''    c.Width = sizes(c)
		'	'' ''Next c
		'	'----------------------------------------------------            
		'End If

#End Region
	End Sub

	Private Sub iReOpen_ItemClick(sender As Object, e As ItemClickEventArgs) Handles iReOpen.ItemClick

		'	Show wait message
		Call ShowWaitFormOnTheButtomCenter("Reading CSV file", "Please wait ...")

		Dim result As String = ""

		Try
			result = Me.ReadFile(Me.inputFileName)

		Catch ex As Exception
			AlertControl1.Show(Me, "Reading CSV file", ex.Message)
			XtraMessageBox.Show(ex.Message, "Error Reading CSV file", MessageBoxButtons.OK, MessageBoxIcon.Error)
		End Try

		Try

			'	Show wait message
			SplashScreenManager.CloseDefaultWaitForm()

		Catch ex As Exception

		End Try


		If Not String.IsNullOrEmpty(result) Then
			AlertControl1.Show(Me, Application.ProductName, result)
		End If

	End Sub

	Private Sub iClose_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles iClose.ItemClick

		gridView1.Columns.Clear()
		gridControl.DataSource = Nothing
		gridControl.RefreshDataSource()

		_crcFileRecordsSplitFields.Clear()
		_crcFileRecordsCommaFields.Clear()

		TxtGeneratorTool.Text = ""
		TxtFileType.Text = ""
		TxtCreationDate.Text = ""
		TxtDataOriginator.Text = ""
		TxtCompanyName.Text = ""
		TxtSourceFileName.Text = ""
		TxtLastModified.Text = ""

		iClose.Enabled = False
		iReOpen.Enabled = False
		iOpen.Enabled = True

		_validCrcRecordsCount = 0
		recordsDataTable.Clear()

		siCrcTotalRecords.Caption = "Total Records: " & recordsDataTable.Rows.Count
		siCrcValidRecords.Caption = "Valid CRC Records: " & _validCrcRecordsCount
		siCrcInvalidRecords.Caption = "Invalid CRC Records: " & recordsDataTable.Rows.Count - _validCrcRecordsCount

		crcValidationProgress.EditValue = RepositoryItemProgressBar1.Minimum

		Me.inputFileName = ""
	End Sub

	''' <summary>
	''' Validate a given CRC file without user interaction, used with program commandline parameter
	''' </summary>
	''' <param name="filename"></param>
	Private Sub AutoValidateCrc(ByVal filename As String)
		'   Read the selected file

		Me.inputFileName = filename

		'	Update the UI panel with the filename
		splitContainerControl.Panel2.Text = "Opened File: " & Me.inputFileName

		'	Show wait message
		Call ShowWaitFormOnTheButtomCenter("Reading CRC file", "Please wait ...")

		Dim result As String = ""

		Try

			result = Me.ReadFile(Me.inputFileName)
		Catch ex As Exception
			AlertControl1.Show(Me, "Reading CRC file", ex.Message)
			XtraMessageBox.Show(ex.Message, "Error Reading CRC file", MessageBoxButtons.OK, MessageBoxIcon.Error)
		End Try

		Try

			'	Show wait message
			SplashScreenManager.CloseDefaultWaitForm()

		Catch ex As Exception

		End Try

		If Not String.IsNullOrEmpty(result) Then
			AlertControl1.Show(Me, Application.ProductName, result)
		End If
	End Sub

	Private Sub gridControl_SizeChanged(ByVal sender As Object, ByVal e As EventArgs) Handles gridControl.SizeChanged
		'  Todo: uncomment these lins to enable adusting the grid column with acc.to the grid width
		'' ''Dim info As GridViewInfo = TryCast(gridView1.GetViewInfo(), GridViewInfo)
		'' ''Dim minWidth As Integer = 0
		'' ''For Each c As GridColumn In gridView1.Columns
		'' ''    minWidth += c.MinWidth
		'' ''Next c

		'' ''gridView1.OptionsView.ColumnAutoWidth = (info.ViewRects.ColumnPanelWidth > minWidth)
	End Sub

	Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

		iClose.Enabled = False
		iReOpen.Enabled = False
		iOpen.Enabled = True

		Me.Text = String.Format("{0} v{1}", Application.ProductName, Application.ProductVersion)

		siStatus.Caption = "CRC File Details: "
		siCrcTotalRecords.Caption = "Total Records: " & recordsDataTable.Rows.Count
		siCrcValidRecords.Caption = "Valid CRC Records: " & _validCrcRecordsCount
		siCrcInvalidRecords.Caption = "Invalid CRC Records: " & recordsDataTable.Rows.Count - _validCrcRecordsCount

		crcValidationProgress.Width = 150

		AlertControl1.ShowPinButton = False

		'gridView1.OptionsView.ColumnAutoWidth = False
		'gridView1.ScrollStyle = ScrollStyleFlags.LiveHorzScroll Or ScrollStyleFlags.LiveVertScroll
		'gridView1.HorzScrollVisibility = ScrollVisibility.Always


		Call LoadCustomLogoOnRibbon(My.Resources.UnitedATS_HeaderLogo98x98)

		'BarEditItem2.EditValue = My.Resources.UATS_Logo_New_small  ' SystemIcons.Exclamation.ToBitmap()

		'BarButtonItem1.edit = My.Resources.UATS_Logo_New_small

		'BarEditItem1.EditValue = My.Resources.UATS_Logo_New_small

	End Sub

	Private Sub FrmMain_Shown(sender As Object, e As EventArgs) Handles Me.Shown
		Dim paramFilename As String = Me.inputFileName

		'	Check for filename in commandline parameter
		If Not String.IsNullOrEmpty(paramFilename) Then
			If File.Exists(paramFilename) Then
				If Path.GetExtension(paramFilename).ToLower().Equals(".crc") Then

					Call AutoValidateCrc(paramFilename)

				Else
					Me.inputFileName = "#"
				End If
			Else
				Me.inputFileName = "#"
			End If
		Else
			'	No commandline param found
			Me.inputFileName = ""
		End If

		If Me.inputFileName.Equals("#") Then
			AlertControl1.Show(Me, "Validating CRC file", "Invalid CRC file")
			XtraMessageBox.Show(String.Format("Invalid CRC file format{0}{1}", vbNewLine, paramFilename), "Error Validating CRC file", MessageBoxButtons.OK, MessageBoxIcon.Error)
			Me.inputFileName = ""
		Else
			Me.inputFileName = paramFilename
		End If
	End Sub

	Private Sub LoadCustomLogoOnRibbon(Optional ByVal imgObj As Image = Nothing, Optional ByVal imgName As String = "")
		Dim skin As Skin = RibbonSkins.GetSkin(DefaultLookAndFeel1.LookAndFeel)

		Dim elem As SkinElement = skin(RibbonSkins.SkinTabPanel)
		elem.Color.BackColor = Color.White
		elem.Image.Stretch = SkinImageStretch.Stretch

		'elem.Image.SizingMargins.Right = 90  ' 98
		'elem.Image.SizingMargins.Left = 0   '6
		'elem.Image.SizingMargins.Top = 80
		'elem.Image.SizingMargins.Bottom = 6 '7

		elem.Image.SizingMargins.Right = 90  ' 98
		elem.Image.SizingMargins.Left = 10   '6
		elem.Image.SizingMargins.Top = 80
		elem.Image.SizingMargins.Bottom = 6 '7

		elem.Image.Layout = SkinImageLayout.Horizontal

		If Not IsNothing(imgObj) Then
			elem.Image.SetImage(imgObj, Color.Transparent)
		ElseIf Not String.IsNullOrEmpty(imgName) Then
			elem.Image.SetImage(imgName, Color.Transparent)
		Else
			elem.Image.SetImage(My.Resources.UnitedATS_HeaderLogo98x98, Color.Transparent)
		End If
	End Sub

	Private Sub iAbout_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles iAbout.ItemClick
		Dim aboutForm As New FrmAbout

		aboutForm.ShowDialog(Me)
	End Sub

	Private Sub AlertControl1_BeforeFormShow(ByVal sender As Object, ByVal e As DevExpress.XtraBars.Alerter.AlertFormEventArgs) Handles AlertControl1.BeforeFormShow
		e.AlertForm.Location = Me.Location
		e.AlertForm.OpacityLevel = 50
	End Sub


	Private Sub ShowWaitFormOnTheButtomCenter(ByVal caption As String, ByVal description As String)
		SplashScreenManager.ShowDefaultWaitForm(Me, True, True, False, 2, caption, description)
		SplashScreenManager.Default.SplashFormStartPosition = SplashFormStartPosition.Manual
		SplashScreenManager.Default.SplashFormLocation = New Point(splitContainerControl.Left + Me.Left + (Me.Width / 2) - 100, splitContainerControl.Top + Me.Top + Me.Height - 300)
		Application.DoEvents
	End Sub

End Class
