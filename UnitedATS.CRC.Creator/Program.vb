﻿Imports DevExpress.Skins
'Imports Rhino.Licensing
'Imports UnitedATS.Security.License

Module Program

	Public Sub Main()
		Dim applicationLicenseFileName As String = IO.Path.GetFileNameWithoutExtension(Application.ExecutablePath)
		Dim hasValidLicense As Boolean = False

		'	Just For Testing
		hasValidLicense = True

		'GoTo SkipLicense

		'Dim applicationLicense As Validator

		'Dim appId As New Guid(CType(My.Application.GetType.Assembly.GetCustomAttributes(GetType(Runtime.InteropServices.
		'					GuidAttribute), False)(0), Runtime.InteropServices.GuidAttribute).Value)


		'applicationLicense = New Validator(My.Resources.UnitedATS_eTOD_Interface_Public, appId.ToString(), applicationLicenseFileName, "Aerodrome.DataSpecs.Validator", LicenseType.Personal, Nothing, "ALL")

		'Try

		'	hasValidLicense = applicationLicense.ChkAppLicense()

		'Catch ex As Rhino.Licensing.LicenseNotFoundException
		'	' License not found
		'	MessageBox.Show("Invalid license, this product is not licensed to run in this computer", "License Validation", MessageBoxButtons.OK, MessageBoxIcon.Error)
		'Catch ex As Rhino.Licensing.LicenseFileNotFoundException
		'	' License file is not found
		'	MessageBox.Show("Unable to locate a license for this product", "License Validation", MessageBoxButtons.OK, MessageBoxIcon.Error)
		'Catch ex As Rhino.Licensing.FloatingLicenseNotAvailableException
		'	' Floating license is not availible
		'	MessageBox.Show("Floating License Not Avialable", "License Validation", MessageBoxButtons.OK, MessageBoxIcon.Error)
		'Catch ex As Rhino.Licensing.LicenseExpiredException
		'	' License expired
		'	MessageBox.Show("License Expired, the trial period for this product has expired", "License Validation", MessageBoxButtons.OK, MessageBoxIcon.Error)
		'Catch ex As Exception
		'	' Unknown error
		'	MessageBox.Show(ex.Message, "License Validation", MessageBoxButtons.OK, MessageBoxIcon.Error)
		'End Try

SkipLicense:

		If hasValidLicense Then
			DevExpress.UserSkins.BonusSkins.Register()
			SkinManager.EnableFormSkins()
			Application.EnableVisualStyles()
			Application.SetCompatibleTextRenderingDefault(False)
			Application.Run(New FrmMain)
		Else
			Application.Exit()
		End If
	End Sub

End Module
