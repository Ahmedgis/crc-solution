﻿Imports DevExpress.Skins
Imports DevExpress.LookAndFeel
Imports DevExpress.UserSkins
Imports DevExpress.XtraBars
Imports DevExpress.XtraBars.Ribbon
Imports DevExpress.XtraBars.Helpers


<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMain
    Inherits RibbonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMain))
		Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
		Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
		Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
		Dim SuperToolTip2 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
		Dim ToolTipTitleItem2 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
		Dim ToolTipItem2 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
		Dim SuperToolTip3 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
		Dim ToolTipTitleItem3 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
		Dim ToolTipItem3 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
		Dim StyleFormatCondition1 As DevExpress.XtraGrid.StyleFormatCondition = New DevExpress.XtraGrid.StyleFormatCondition()
		Me.BarEditItem1 = New DevExpress.XtraBars.BarEditItem()
		Me.RepositoryItemPictureEdit4 = New DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit()
		Me.splitContainerControl = New DevExpress.XtraEditors.SplitContainerControl()
		Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
		Me.TxtSourceFileName = New DevExpress.XtraEditors.TextEdit()
		Me.ribbonControl = New DevExpress.XtraBars.Ribbon.RibbonControl()
		Me.appMenu = New DevExpress.XtraBars.Ribbon.ApplicationMenu()
		Me.iOpen = New DevExpress.XtraBars.BarButtonItem()
		Me.iClose = New DevExpress.XtraBars.BarButtonItem()
		Me.iAbout = New DevExpress.XtraBars.BarButtonItem()
		Me.iExit = New DevExpress.XtraBars.BarButtonItem()
		Me.ribbonImageCollection = New DevExpress.Utils.ImageCollection()
		Me.iHelp = New DevExpress.XtraBars.BarButtonItem()
		Me.siStatus = New DevExpress.XtraBars.BarStaticItem()
		Me.siCrcTotalRecords = New DevExpress.XtraBars.BarStaticItem()
		Me.alignButtonGroup = New DevExpress.XtraBars.BarButtonGroup()
		Me.iBoldFontStyle = New DevExpress.XtraBars.BarButtonItem()
		Me.iItalicFontStyle = New DevExpress.XtraBars.BarButtonItem()
		Me.iUnderlinedFontStyle = New DevExpress.XtraBars.BarButtonItem()
		Me.fontStyleButtonGroup = New DevExpress.XtraBars.BarButtonGroup()
		Me.iLeftTextAlign = New DevExpress.XtraBars.BarButtonItem()
		Me.iCenterTextAlign = New DevExpress.XtraBars.BarButtonItem()
		Me.iRightTextAlign = New DevExpress.XtraBars.BarButtonItem()
		Me.rgbiSkins = New DevExpress.XtraBars.RibbonGalleryBarItem()
		Me.siCrcValidRecords = New DevExpress.XtraBars.BarStaticItem()
		Me.siCrcInvalidRecords = New DevExpress.XtraBars.BarStaticItem()
		Me.crcValidationProgress = New DevExpress.XtraBars.BarEditItem()
		Me.RepositoryItemProgressBar1 = New DevExpress.XtraEditors.Repository.RepositoryItemProgressBar()
		Me.BarEditItem2 = New DevExpress.XtraBars.BarEditItem()
		Me.RepositoryItemPictureEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit()
		Me.BarEditItem3 = New DevExpress.XtraBars.BarEditItem()
		Me.RepositoryItemImageEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemImageEdit()
		Me.iReRead = New DevExpress.XtraBars.BarButtonItem()
		Me.iGenerateCrc = New DevExpress.XtraBars.BarButtonItem()
		Me.iExportCrc = New DevExpress.XtraBars.BarButtonItem()
		Me.iExportCsv = New DevExpress.XtraBars.BarButtonItem()
		Me.ribbonImageCollectionLarge = New DevExpress.Utils.ImageCollection()
		Me.homeRibbonPage = New DevExpress.XtraBars.Ribbon.RibbonPage()
		Me.fileRibbonPageGroup = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
		Me.RibbonPageGroupGenerateCrc = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
		Me.exitRibbonPageGroup = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
		Me.helpRibbonPage = New DevExpress.XtraBars.Ribbon.RibbonPage()
		Me.helpRibbonPageGroup = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
		Me.RepositoryItemImageEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemImageEdit()
		Me.RepositoryItemImageEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemImageEdit()
		Me.RepositoryItemPictureEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit()
		Me.RepositoryItemPictureEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit()
		Me.ribbonStatusBar = New DevExpress.XtraBars.Ribbon.RibbonStatusBar()
		Me.TxtLastModified = New DevExpress.XtraEditors.TextEdit()
		Me.TxtCreationDate = New DevExpress.XtraEditors.TextEdit()
		Me.TxtCompanyName = New DevExpress.XtraEditors.TextEdit()
		Me.TxtDataOriginator = New DevExpress.XtraEditors.TextEdit()
		Me.TxtGeneratorTool = New DevExpress.XtraEditors.TextEdit()
		Me.TxtFileType = New DevExpress.XtraEditors.TextEdit()
		Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
		Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
		Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
		Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
		Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
		Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
		Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
		Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
		Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
		Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
		Me.gridControl = New DevExpress.XtraGrid.GridControl()
		Me.gridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
		Me.popupControlContainer2 = New DevExpress.XtraBars.PopupControlContainer()
		Me.buttonEdit = New DevExpress.XtraEditors.ButtonEdit()
		Me.popupControlContainer1 = New DevExpress.XtraBars.PopupControlContainer()
		Me.someLabelControl2 = New DevExpress.XtraEditors.LabelControl()
		Me.someLabelControl1 = New DevExpress.XtraEditors.LabelControl()
		Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
		Me.ToolTipController1 = New DevExpress.Utils.ToolTipController()
		Me.AlertControl1 = New DevExpress.XtraBars.Alerter.AlertControl()
		Me.DefaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel()
		CType(Me.RepositoryItemPictureEdit4,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.splitContainerControl,System.ComponentModel.ISupportInitialize).BeginInit
		Me.splitContainerControl.SuspendLayout
		CType(Me.LayoutControl1,System.ComponentModel.ISupportInitialize).BeginInit
		Me.LayoutControl1.SuspendLayout
		CType(Me.TxtSourceFileName.Properties,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.ribbonControl,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.appMenu,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.ribbonImageCollection,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.RepositoryItemProgressBar1,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.RepositoryItemPictureEdit3,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.RepositoryItemImageEdit3,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.ribbonImageCollectionLarge,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.RepositoryItemImageEdit1,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.RepositoryItemImageEdit2,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.RepositoryItemPictureEdit1,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.RepositoryItemPictureEdit2,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.TxtLastModified.Properties,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.TxtCreationDate.Properties,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.TxtCompanyName.Properties,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.TxtDataOriginator.Properties,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.TxtGeneratorTool.Properties,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.TxtFileType.Properties,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.LayoutControlGroup1,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.LayoutControlGroup2,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.LayoutControlItem3,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.LayoutControlItem1,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.LayoutControlItem2,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.LayoutControlItem6,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.LayoutControlItem5,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.LayoutControlItem4,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.LayoutControlItem7,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.EmptySpaceItem1,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.gridControl,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.gridView1,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.popupControlContainer2,System.ComponentModel.ISupportInitialize).BeginInit
		Me.popupControlContainer2.SuspendLayout
		CType(Me.buttonEdit.Properties,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.popupControlContainer1,System.ComponentModel.ISupportInitialize).BeginInit
		Me.popupControlContainer1.SuspendLayout
		Me.SuspendLayout
		'
		'BarEditItem1
		'
		Me.BarEditItem1.AllowRightClickInMenu = false
		Me.BarEditItem1.AutoHideEdit = false
		Me.BarEditItem1.CanOpenEdit = false
		Me.BarEditItem1.Edit = Me.RepositoryItemPictureEdit4
		Me.BarEditItem1.EditHeight = 80
		Me.BarEditItem1.Id = 74
		Me.BarEditItem1.LargeImageIndex = 4
		Me.BarEditItem1.Name = "BarEditItem1"
		Me.BarEditItem1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large
		'
		'RepositoryItemPictureEdit4
		'
		Me.RepositoryItemPictureEdit4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
		Me.RepositoryItemPictureEdit4.Name = "RepositoryItemPictureEdit4"
		Me.RepositoryItemPictureEdit4.PictureInterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Bicubic
		Me.RepositoryItemPictureEdit4.ReadOnly = true
		Me.RepositoryItemPictureEdit4.ShowMenu = false
		Me.RepositoryItemPictureEdit4.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch
		'
		'splitContainerControl
		'
		Me.splitContainerControl.Dock = System.Windows.Forms.DockStyle.Fill
		Me.splitContainerControl.Horizontal = false
		Me.splitContainerControl.Location = New System.Drawing.Point(0, 146)
		Me.splitContainerControl.Name = "splitContainerControl"
		Me.splitContainerControl.Padding = New System.Windows.Forms.Padding(6)
		Me.splitContainerControl.Panel1.Controls.Add(Me.LayoutControl1)
		Me.splitContainerControl.Panel1.Text = "Panel1"
		Me.splitContainerControl.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.[Default]
		Me.splitContainerControl.Panel2.Controls.Add(Me.gridControl)
		Me.splitContainerControl.Panel2.ShowCaption = true
		Me.splitContainerControl.Panel2.Text = "File Contents"
		Me.splitContainerControl.PanelVisibility = DevExpress.XtraEditors.SplitPanelVisibility.Panel2
		Me.splitContainerControl.Size = New System.Drawing.Size(953, 481)
		Me.splitContainerControl.SplitterPosition = 0
		Me.splitContainerControl.TabIndex = 1
		Me.splitContainerControl.Text = "splitContainerControl1"
		'
		'LayoutControl1
		'
		Me.LayoutControl1.Controls.Add(Me.TxtSourceFileName)
		Me.LayoutControl1.Controls.Add(Me.TxtLastModified)
		Me.LayoutControl1.Controls.Add(Me.TxtCreationDate)
		Me.LayoutControl1.Controls.Add(Me.TxtCompanyName)
		Me.LayoutControl1.Controls.Add(Me.TxtDataOriginator)
		Me.LayoutControl1.Controls.Add(Me.TxtGeneratorTool)
		Me.LayoutControl1.Controls.Add(Me.TxtFileType)
		Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
		Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
		Me.LayoutControl1.Name = "LayoutControl1"
		Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(1080, 122, 250, 350)
		Me.LayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.[True]
		Me.LayoutControl1.Root = Me.LayoutControlGroup1
		Me.LayoutControl1.Size = New System.Drawing.Size(0, 0)
		Me.LayoutControl1.TabIndex = 0
		Me.LayoutControl1.Text = "LayoutControl1"
		'
		'TxtSourceFileName
		'
		Me.TxtSourceFileName.Location = New System.Drawing.Point(100, 111)
		Me.TxtSourceFileName.MenuManager = Me.ribbonControl
		Me.TxtSourceFileName.Name = "TxtSourceFileName"
		Me.TxtSourceFileName.Properties.ReadOnly = true
		Me.TxtSourceFileName.Size = New System.Drawing.Size(50, 20)
		Me.TxtSourceFileName.StyleController = Me.LayoutControl1
		Me.TxtSourceFileName.TabIndex = 11
		'
		'ribbonControl
		'
		Me.ribbonControl.ApplicationButtonDropDownControl = Me.appMenu
		Me.ribbonControl.ApplicationButtonText = Nothing
		Me.ribbonControl.ExpandCollapseItem.Id = 0
		Me.ribbonControl.Images = Me.ribbonImageCollection
		Me.ribbonControl.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.ribbonControl.ExpandCollapseItem, Me.iOpen, Me.iClose, Me.iExit, Me.iHelp, Me.iAbout, Me.siStatus, Me.siCrcTotalRecords, Me.alignButtonGroup, Me.iBoldFontStyle, Me.iItalicFontStyle, Me.iUnderlinedFontStyle, Me.fontStyleButtonGroup, Me.iLeftTextAlign, Me.iCenterTextAlign, Me.iRightTextAlign, Me.rgbiSkins, Me.siCrcValidRecords, Me.siCrcInvalidRecords, Me.crcValidationProgress, Me.BarEditItem2, Me.BarEditItem3, Me.BarEditItem1, Me.iReRead, Me.iGenerateCrc, Me.iExportCrc, Me.iExportCsv})
		Me.ribbonControl.LargeImages = Me.ribbonImageCollectionLarge
		Me.ribbonControl.Location = New System.Drawing.Point(0, 0)
		Me.ribbonControl.MaxItemId = 81
		Me.ribbonControl.Name = "ribbonControl"
		Me.ribbonControl.PageHeaderItemLinks.Add(Me.iAbout)
		Me.ribbonControl.Pages.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPage() {Me.homeRibbonPage, Me.helpRibbonPage})
		Me.ribbonControl.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemImageEdit1, Me.RepositoryItemProgressBar1, Me.RepositoryItemImageEdit2, Me.RepositoryItemPictureEdit1, Me.RepositoryItemPictureEdit2, Me.RepositoryItemPictureEdit3, Me.RepositoryItemImageEdit3, Me.RepositoryItemPictureEdit4})
		Me.ribbonControl.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2010
		Me.ribbonControl.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.[False]
		Me.ribbonControl.ShowToolbarCustomizeItem = false
		Me.ribbonControl.Size = New System.Drawing.Size(953, 146)
		Me.ribbonControl.StatusBar = Me.ribbonStatusBar
		Me.ribbonControl.Toolbar.ItemLinks.Add(Me.iOpen)
		Me.ribbonControl.Toolbar.ItemLinks.Add(Me.iHelp)
		Me.ribbonControl.Toolbar.ShowCustomizeItem = false
		Me.ribbonControl.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden
		'
		'appMenu
		'
		Me.appMenu.ItemLinks.Add(Me.iOpen)
		Me.appMenu.ItemLinks.Add(Me.iClose)
		Me.appMenu.ItemLinks.Add(Me.iAbout, true)
		Me.appMenu.ItemLinks.Add(Me.iExit, true)
		Me.appMenu.Name = "appMenu"
		Me.appMenu.Ribbon = Me.ribbonControl
		'
		'iOpen
		'
		Me.iOpen.Caption = "&Open && Read"
		Me.iOpen.Description = "Opens file and read its contents."
		Me.iOpen.Glyph = CType(resources.GetObject("iOpen.Glyph"),System.Drawing.Image)
		Me.iOpen.Hint = "Opens a CSV file"
		Me.iOpen.Id = 2
		Me.iOpen.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.O))
		Me.iOpen.LargeGlyph = CType(resources.GetObject("iOpen.LargeGlyph"),System.Drawing.Image)
		Me.iOpen.LargeImageIndex = 1
		Me.iOpen.Name = "iOpen"
		Me.iOpen.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large
		ToolTipTitleItem1.Text = "Open && Read"
		ToolTipItem1.LeftIndent = 6
		ToolTipItem1.Text = "Open && Read file contents"
		SuperToolTip1.Items.Add(ToolTipTitleItem1)
		SuperToolTip1.Items.Add(ToolTipItem1)
		Me.iOpen.SuperTip = SuperToolTip1
		'
		'iClose
		'
		Me.iClose.Caption = "&Close"
		Me.iClose.Description = "Closes the active CRC File."
		Me.iClose.Hint = "Closes a CRC file"
		Me.iClose.Id = 3
		Me.iClose.ImageIndex = 2
		Me.iClose.LargeImageIndex = 2
		Me.iClose.Name = "iClose"
		Me.iClose.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large
		'
		'iAbout
		'
		Me.iAbout.Caption = "About"
		Me.iAbout.Description = "Displays general program information."
		Me.iAbout.Hint = "Displays general program information"
		Me.iAbout.Id = 24
		Me.iAbout.ImageIndex = 8
		Me.iAbout.LargeImageIndex = 8
		Me.iAbout.Name = "iAbout"
		'
		'iExit
		'
		Me.iExit.Caption = "Exit"
		Me.iExit.Description = "Closes this program after prompting you to save unsaved data."
		Me.iExit.Hint = "Closes this program after prompting you"
		Me.iExit.Id = 20
		Me.iExit.ImageIndex = 6
		Me.iExit.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.X))
		Me.iExit.LargeImageIndex = 6
		Me.iExit.Name = "iExit"
		'
		'ribbonImageCollection
		'
		Me.ribbonImageCollection.ImageStream = CType(resources.GetObject("ribbonImageCollection.ImageStream"),DevExpress.Utils.ImageCollectionStreamer)
		Me.ribbonImageCollection.Images.SetKeyName(0, "Ribbon_New_16x16.png")
		Me.ribbonImageCollection.Images.SetKeyName(1, "Ribbon_Open_16x16.png")
		Me.ribbonImageCollection.Images.SetKeyName(2, "Ribbon_Close_16x16.png")
		Me.ribbonImageCollection.Images.SetKeyName(3, "Ribbon_Find_16x16.png")
		Me.ribbonImageCollection.Images.SetKeyName(4, "Ribbon_Save_16x16.png")
		Me.ribbonImageCollection.Images.SetKeyName(5, "Ribbon_SaveAs_16x16.png")
		Me.ribbonImageCollection.Images.SetKeyName(6, "Ribbon_Exit_16x16.png")
		Me.ribbonImageCollection.Images.SetKeyName(7, "Ribbon_Content_16x16.png")
		Me.ribbonImageCollection.Images.SetKeyName(8, "Ribbon_Info_16x16.png")
		Me.ribbonImageCollection.Images.SetKeyName(9, "Ribbon_Bold_16x16.png")
		Me.ribbonImageCollection.Images.SetKeyName(10, "Ribbon_Italic_16x16.png")
		Me.ribbonImageCollection.Images.SetKeyName(11, "Ribbon_Underline_16x16.png")
		Me.ribbonImageCollection.Images.SetKeyName(12, "Ribbon_AlignLeft_16x16.png")
		Me.ribbonImageCollection.Images.SetKeyName(13, "Ribbon_AlignCenter_16x16.png")
		Me.ribbonImageCollection.Images.SetKeyName(14, "Ribbon_AlignRight_16x16.png")
		'
		'iHelp
		'
		Me.iHelp.Caption = "Help"
		Me.iHelp.Description = "Start the program help system."
		Me.iHelp.Hint = "Start the program help system"
		Me.iHelp.Id = 22
		Me.iHelp.ImageIndex = 7
		Me.iHelp.LargeImageIndex = 7
		Me.iHelp.Name = "iHelp"
		'
		'siStatus
		'
		Me.siStatus.Caption = "file details:"
		Me.siStatus.Id = 31
		Me.siStatus.Name = "siStatus"
		Me.siStatus.TextAlignment = System.Drawing.StringAlignment.Near
		'
		'siCrcTotalRecords
		'
		Me.siCrcTotalRecords.Caption = "Total Records:"
		Me.siCrcTotalRecords.Glyph = Global.My.Resources.Resources.document_index
		Me.siCrcTotalRecords.Hint = "Total File Records"
		Me.siCrcTotalRecords.Id = 32
		Me.siCrcTotalRecords.Name = "siCrcTotalRecords"
		Me.siCrcTotalRecords.TextAlignment = System.Drawing.StringAlignment.Near
		'
		'alignButtonGroup
		'
		Me.alignButtonGroup.Caption = "Align Commands"
		Me.alignButtonGroup.Id = 52
		Me.alignButtonGroup.ItemLinks.Add(Me.iBoldFontStyle)
		Me.alignButtonGroup.ItemLinks.Add(Me.iItalicFontStyle)
		Me.alignButtonGroup.ItemLinks.Add(Me.iUnderlinedFontStyle)
		Me.alignButtonGroup.Name = "alignButtonGroup"
		'
		'iBoldFontStyle
		'
		Me.iBoldFontStyle.Caption = "Bold"
		Me.iBoldFontStyle.Id = 53
		Me.iBoldFontStyle.ImageIndex = 9
		Me.iBoldFontStyle.Name = "iBoldFontStyle"
		'
		'iItalicFontStyle
		'
		Me.iItalicFontStyle.Caption = "Italic"
		Me.iItalicFontStyle.Id = 54
		Me.iItalicFontStyle.ImageIndex = 10
		Me.iItalicFontStyle.Name = "iItalicFontStyle"
		'
		'iUnderlinedFontStyle
		'
		Me.iUnderlinedFontStyle.Caption = "Underlined"
		Me.iUnderlinedFontStyle.Id = 55
		Me.iUnderlinedFontStyle.ImageIndex = 11
		Me.iUnderlinedFontStyle.Name = "iUnderlinedFontStyle"
		'
		'fontStyleButtonGroup
		'
		Me.fontStyleButtonGroup.Caption = "Font Style"
		Me.fontStyleButtonGroup.Id = 56
		Me.fontStyleButtonGroup.ItemLinks.Add(Me.iLeftTextAlign)
		Me.fontStyleButtonGroup.ItemLinks.Add(Me.iCenterTextAlign)
		Me.fontStyleButtonGroup.ItemLinks.Add(Me.iRightTextAlign)
		Me.fontStyleButtonGroup.Name = "fontStyleButtonGroup"
		'
		'iLeftTextAlign
		'
		Me.iLeftTextAlign.Caption = "Left"
		Me.iLeftTextAlign.Id = 57
		Me.iLeftTextAlign.ImageIndex = 12
		Me.iLeftTextAlign.Name = "iLeftTextAlign"
		'
		'iCenterTextAlign
		'
		Me.iCenterTextAlign.Caption = "Center"
		Me.iCenterTextAlign.Id = 58
		Me.iCenterTextAlign.ImageIndex = 13
		Me.iCenterTextAlign.Name = "iCenterTextAlign"
		'
		'iRightTextAlign
		'
		Me.iRightTextAlign.Caption = "Right"
		Me.iRightTextAlign.Id = 59
		Me.iRightTextAlign.ImageIndex = 14
		Me.iRightTextAlign.Name = "iRightTextAlign"
		'
		'rgbiSkins
		'
		Me.rgbiSkins.Caption = "Skins"
		'
		'
		'
		Me.rgbiSkins.Gallery.AllowHoverImages = true
		Me.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Normal.Options.UseFont = true
		Me.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Normal.Options.UseTextOptions = true
		Me.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
		Me.rgbiSkins.Gallery.ColumnCount = 4
		Me.rgbiSkins.Gallery.FixedHoverImageSize = false
		Me.rgbiSkins.Gallery.ImageSize = New System.Drawing.Size(32, 17)
		Me.rgbiSkins.Gallery.ItemImageLocation = DevExpress.Utils.Locations.Top
		Me.rgbiSkins.Gallery.RowCount = 4
		Me.rgbiSkins.Id = 60
		Me.rgbiSkins.Name = "rgbiSkins"
		'
		'siCrcValidRecords
		'
		Me.siCrcValidRecords.Caption = "Generated CRC Records:"
		Me.siCrcValidRecords.Glyph = Global.My.Resources.Resources.accept
		Me.siCrcValidRecords.Hint = "Generated CRC Records"
		Me.siCrcValidRecords.Id = 62
		Me.siCrcValidRecords.Name = "siCrcValidRecords"
		Me.siCrcValidRecords.TextAlignment = System.Drawing.StringAlignment.Near
		'
		'siCrcInvalidRecords
		'
		Me.siCrcInvalidRecords.Caption = "Ungenerated CRC Records:"
		Me.siCrcInvalidRecords.Glyph = Global.My.Resources.Resources.cancel
		Me.siCrcInvalidRecords.Hint = "Ungenerated CRC Records"
		Me.siCrcInvalidRecords.Id = 63
		Me.siCrcInvalidRecords.Name = "siCrcInvalidRecords"
		Me.siCrcInvalidRecords.TextAlignment = System.Drawing.StringAlignment.Near
		'
		'crcValidationProgress
		'
		Me.crcValidationProgress.AllowRightClickInMenu = false
		Me.crcValidationProgress.CanOpenEdit = false
		Me.crcValidationProgress.Caption = "Progress"
		Me.crcValidationProgress.Edit = Me.RepositoryItemProgressBar1
		Me.crcValidationProgress.EditWidth = 150
		Me.crcValidationProgress.Glyph = Global.My.Resources.Resources.progressbar
		Me.crcValidationProgress.Hint = "CRC Validation Operation Progress"
		Me.crcValidationProgress.Id = 66
		Me.crcValidationProgress.Name = "crcValidationProgress"
		'
		'RepositoryItemProgressBar1
		'
		Me.RepositoryItemProgressBar1.FlowAnimationEnabled = true
		Me.RepositoryItemProgressBar1.LookAndFeel.UseDefaultLookAndFeel = false
		Me.RepositoryItemProgressBar1.Name = "RepositoryItemProgressBar1"
		Me.RepositoryItemProgressBar1.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid
		Me.RepositoryItemProgressBar1.ReadOnly = true
		Me.RepositoryItemProgressBar1.ShowTitle = true
		Me.RepositoryItemProgressBar1.Step = 1
		'
		'BarEditItem2
		'
		Me.BarEditItem2.AllowRightClickInMenu = false
		Me.BarEditItem2.AutoHideEdit = false
		Me.BarEditItem2.CanOpenEdit = false
		Me.BarEditItem2.Edit = Me.RepositoryItemPictureEdit3
		Me.BarEditItem2.Id = 70
		Me.BarEditItem2.Name = "BarEditItem2"
		Me.BarEditItem2.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large
		'
		'RepositoryItemPictureEdit3
		'
		Me.RepositoryItemPictureEdit3.Name = "RepositoryItemPictureEdit3"
		Me.RepositoryItemPictureEdit3.ReadOnly = true
		Me.RepositoryItemPictureEdit3.ShowMenu = false
		Me.RepositoryItemPictureEdit3.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch
		'
		'BarEditItem3
		'
		Me.BarEditItem3.Caption = "BarEditItem3"
		Me.BarEditItem3.Edit = Me.RepositoryItemImageEdit3
		Me.BarEditItem3.Id = 71
		Me.BarEditItem3.Name = "BarEditItem3"
		'
		'RepositoryItemImageEdit3
		'
		Me.RepositoryItemImageEdit3.AutoHeight = false
		Me.RepositoryItemImageEdit3.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.RepositoryItemImageEdit3.Name = "RepositoryItemImageEdit3"
		'
		'iReRead
		'
		Me.iReRead.Caption = "Re-Read"
		Me.iReRead.Glyph = CType(resources.GetObject("iReRead.Glyph"),System.Drawing.Image)
		Me.iReRead.Id = 75
		Me.iReRead.LargeGlyph = CType(resources.GetObject("iReRead.LargeGlyph"),System.Drawing.Image)
		Me.iReRead.Name = "iReRead"
		ToolTipTitleItem2.Text = "Re-Read File"
		ToolTipItem2.LeftIndent = 6
		ToolTipItem2.Text = "Re-Read current selected file"
		SuperToolTip2.Items.Add(ToolTipTitleItem2)
		SuperToolTip2.Items.Add(ToolTipItem2)
		Me.iReRead.SuperTip = SuperToolTip2
		'
		'iGenerateCrc
		'
		Me.iGenerateCrc.Caption = "Generate CRC"
		Me.iGenerateCrc.Glyph = CType(resources.GetObject("iGenerateCrc.Glyph"),System.Drawing.Image)
		Me.iGenerateCrc.Id = 77
		Me.iGenerateCrc.LargeGlyph = CType(resources.GetObject("iGenerateCrc.LargeGlyph"),System.Drawing.Image)
		Me.iGenerateCrc.Name = "iGenerateCrc"
		'
		'iExportCrc
		'
		Me.iExportCrc.Caption = "Export to CRC"
		Me.iExportCrc.Glyph = CType(resources.GetObject("iExportCrc.Glyph"),System.Drawing.Image)
		Me.iExportCrc.Id = 79
		Me.iExportCrc.LargeGlyph = CType(resources.GetObject("iExportCrc.LargeGlyph"),System.Drawing.Image)
		Me.iExportCrc.Name = "iExportCrc"
		ToolTipTitleItem3.Text = "Export CRC File"
		ToolTipItem3.LeftIndent = 6
		ToolTipItem3.Text = "Export the current data after generating the CRC values for all the records to a "& _ 
    "new CRC file with the same input file name"
		SuperToolTip3.Items.Add(ToolTipTitleItem3)
		SuperToolTip3.Items.Add(ToolTipItem3)
		Me.iExportCrc.SuperTip = SuperToolTip3
		'
		'iExportCsv
		'
		Me.iExportCsv.Caption = "Export to CSV"
		Me.iExportCsv.Glyph = CType(resources.GetObject("iExportCsv.Glyph"),System.Drawing.Image)
		Me.iExportCsv.Id = 80
		Me.iExportCsv.LargeGlyph = CType(resources.GetObject("iExportCsv.LargeGlyph"),System.Drawing.Image)
		Me.iExportCsv.Name = "iExportCsv"
		Me.iExportCsv.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
		'
		'ribbonImageCollectionLarge
		'
		Me.ribbonImageCollectionLarge.ImageSize = New System.Drawing.Size(32, 32)
		Me.ribbonImageCollectionLarge.ImageStream = CType(resources.GetObject("ribbonImageCollectionLarge.ImageStream"),DevExpress.Utils.ImageCollectionStreamer)
		Me.ribbonImageCollectionLarge.Images.SetKeyName(0, "Ribbon_New_32x32.png")
		Me.ribbonImageCollectionLarge.Images.SetKeyName(1, "Ribbon_Open_32x32.png")
		Me.ribbonImageCollectionLarge.Images.SetKeyName(2, "Ribbon_Close_32x32.png")
		Me.ribbonImageCollectionLarge.Images.SetKeyName(3, "Ribbon_Find_32x32.png")
		Me.ribbonImageCollectionLarge.Images.SetKeyName(4, "Ribbon_Save_32x32.png")
		Me.ribbonImageCollectionLarge.Images.SetKeyName(5, "Ribbon_SaveAs_32x32.png")
		Me.ribbonImageCollectionLarge.Images.SetKeyName(6, "Ribbon_Exit_32x32.png")
		Me.ribbonImageCollectionLarge.Images.SetKeyName(7, "Ribbon_Content_32x32.png")
		Me.ribbonImageCollectionLarge.Images.SetKeyName(8, "Ribbon_Info_32x32.png")
		'
		'homeRibbonPage
		'
		Me.homeRibbonPage.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.fileRibbonPageGroup, Me.RibbonPageGroupGenerateCrc, Me.exitRibbonPageGroup})
		Me.homeRibbonPage.Name = "homeRibbonPage"
		Me.homeRibbonPage.Text = "Home"
		'
		'fileRibbonPageGroup
		'
		Me.fileRibbonPageGroup.ItemLinks.Add(Me.iOpen)
		Me.fileRibbonPageGroup.ItemLinks.Add(Me.iReRead)
		Me.fileRibbonPageGroup.ItemLinks.Add(Me.iClose, true)
		Me.fileRibbonPageGroup.Name = "fileRibbonPageGroup"
		Me.fileRibbonPageGroup.Text = "CSV File Operations"
		'
		'RibbonPageGroupGenerateCrc
		'
		Me.RibbonPageGroupGenerateCrc.AllowTextClipping = false
		Me.RibbonPageGroupGenerateCrc.ItemLinks.Add(Me.iGenerateCrc)
		Me.RibbonPageGroupGenerateCrc.ItemLinks.Add(Me.iExportCrc, true)
		Me.RibbonPageGroupGenerateCrc.ItemLinks.Add(Me.iExportCsv)
		Me.RibbonPageGroupGenerateCrc.Name = "RibbonPageGroupGenerateCrc"
		Me.RibbonPageGroupGenerateCrc.Text = "Operations"
		'
		'exitRibbonPageGroup
		'
		Me.exitRibbonPageGroup.ItemLinks.Add(Me.iAbout, true)
		Me.exitRibbonPageGroup.ItemLinks.Add(Me.iExit)
		Me.exitRibbonPageGroup.Name = "exitRibbonPageGroup"
		Me.exitRibbonPageGroup.Text = "Exit"
		'
		'helpRibbonPage
		'
		Me.helpRibbonPage.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.helpRibbonPageGroup})
		Me.helpRibbonPage.Name = "helpRibbonPage"
		Me.helpRibbonPage.Text = "About"
		Me.helpRibbonPage.Visible = false
		'
		'helpRibbonPageGroup
		'
		Me.helpRibbonPageGroup.ItemLinks.Add(Me.iHelp)
		Me.helpRibbonPageGroup.Name = "helpRibbonPageGroup"
		Me.helpRibbonPageGroup.Text = "Help"
		'
		'RepositoryItemImageEdit1
		'
		Me.RepositoryItemImageEdit1.AutoHeight = false
		Me.RepositoryItemImageEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.RepositoryItemImageEdit1.Name = "RepositoryItemImageEdit1"
		'
		'RepositoryItemImageEdit2
		'
		Me.RepositoryItemImageEdit2.AutoHeight = false
		Me.RepositoryItemImageEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.RepositoryItemImageEdit2.Name = "RepositoryItemImageEdit2"
		'
		'RepositoryItemPictureEdit1
		'
		Me.RepositoryItemPictureEdit1.Name = "RepositoryItemPictureEdit1"
		Me.RepositoryItemPictureEdit1.ReadOnly = true
		'
		'RepositoryItemPictureEdit2
		'
		Me.RepositoryItemPictureEdit2.Name = "RepositoryItemPictureEdit2"
		'
		'ribbonStatusBar
		'
		Me.ribbonStatusBar.ItemLinks.Add(Me.siStatus)
		Me.ribbonStatusBar.ItemLinks.Add(Me.siCrcTotalRecords)
		Me.ribbonStatusBar.ItemLinks.Add(Me.siCrcValidRecords)
		Me.ribbonStatusBar.ItemLinks.Add(Me.siCrcInvalidRecords)
		Me.ribbonStatusBar.ItemLinks.Add(Me.crcValidationProgress)
		Me.ribbonStatusBar.Location = New System.Drawing.Point(0, 627)
		Me.ribbonStatusBar.Name = "ribbonStatusBar"
		Me.ribbonStatusBar.Ribbon = Me.ribbonControl
		Me.ribbonStatusBar.Size = New System.Drawing.Size(953, 28)
		'
		'TxtLastModified
		'
		Me.TxtLastModified.Location = New System.Drawing.Point(235, 87)
		Me.TxtLastModified.MenuManager = Me.ribbonControl
		Me.TxtLastModified.Name = "TxtLastModified"
		Me.TxtLastModified.Properties.ReadOnly = true
		Me.TxtLastModified.Size = New System.Drawing.Size(50, 20)
		Me.TxtLastModified.StyleController = Me.LayoutControl1
		Me.TxtLastModified.TabIndex = 10
		'
		'TxtCreationDate
		'
		Me.TxtCreationDate.Location = New System.Drawing.Point(235, 63)
		Me.TxtCreationDate.MenuManager = Me.ribbonControl
		Me.TxtCreationDate.Name = "TxtCreationDate"
		Me.TxtCreationDate.Properties.ReadOnly = true
		Me.TxtCreationDate.Size = New System.Drawing.Size(50, 20)
		Me.TxtCreationDate.StyleController = Me.LayoutControl1
		Me.TxtCreationDate.TabIndex = 9
		'
		'TxtCompanyName
		'
		Me.TxtCompanyName.Location = New System.Drawing.Point(235, 39)
		Me.TxtCompanyName.MenuManager = Me.ribbonControl
		Me.TxtCompanyName.Name = "TxtCompanyName"
		Me.TxtCompanyName.Properties.ReadOnly = true
		Me.TxtCompanyName.Size = New System.Drawing.Size(50, 20)
		Me.TxtCompanyName.StyleController = Me.LayoutControl1
		Me.TxtCompanyName.TabIndex = 8
		'
		'TxtDataOriginator
		'
		Me.TxtDataOriginator.Location = New System.Drawing.Point(100, 87)
		Me.TxtDataOriginator.MenuManager = Me.ribbonControl
		Me.TxtDataOriginator.Name = "TxtDataOriginator"
		Me.TxtDataOriginator.Properties.ReadOnly = true
		Me.TxtDataOriginator.Size = New System.Drawing.Size(50, 20)
		Me.TxtDataOriginator.StyleController = Me.LayoutControl1
		Me.TxtDataOriginator.TabIndex = 7
		'
		'TxtGeneratorTool
		'
		Me.TxtGeneratorTool.Location = New System.Drawing.Point(100, 39)
		Me.TxtGeneratorTool.MenuManager = Me.ribbonControl
		Me.TxtGeneratorTool.Name = "TxtGeneratorTool"
		Me.TxtGeneratorTool.Properties.ReadOnly = true
		Me.TxtGeneratorTool.Size = New System.Drawing.Size(50, 20)
		Me.TxtGeneratorTool.StyleController = Me.LayoutControl1
		Me.TxtGeneratorTool.TabIndex = 6
		'
		'TxtFileType
		'
		Me.TxtFileType.Location = New System.Drawing.Point(100, 63)
		Me.TxtFileType.MenuManager = Me.ribbonControl
		Me.TxtFileType.Name = "TxtFileType"
		Me.TxtFileType.Properties.ReadOnly = true
		Me.TxtFileType.Size = New System.Drawing.Size(50, 20)
		Me.TxtFileType.StyleController = Me.LayoutControl1
		Me.TxtFileType.TabIndex = 5
		'
		'LayoutControlGroup1
		'
		Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
		Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
		Me.LayoutControlGroup1.GroupBordersVisible = false
		Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup2})
		Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
		Me.LayoutControlGroup1.Name = "Root"
		Me.LayoutControlGroup1.OptionsToolTip.IconToolTipIconType = DevExpress.Utils.ToolTipIconType.Application
		Me.LayoutControlGroup1.OptionsToolTip.IconToolTipTitle = "CRC File Header Information"
		Me.LayoutControlGroup1.OptionsToolTip.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Application
		Me.LayoutControlGroup1.OptionsToolTip.ToolTipTitle = "CRC File Header Information"
		Me.LayoutControlGroup1.Padding = New DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5)
		Me.LayoutControlGroup1.Size = New System.Drawing.Size(304, 150)
		Me.LayoutControlGroup1.TextVisible = false
		'
		'LayoutControlGroup2
		'
		Me.LayoutControlGroup2.CustomizationFormText = "CRC File header information"
		Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem3, Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem6, Me.LayoutControlItem5, Me.LayoutControlItem4, Me.LayoutControlItem7, Me.EmptySpaceItem1})
		Me.LayoutControlGroup2.Location = New System.Drawing.Point(0, 0)
		Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
		Me.LayoutControlGroup2.Size = New System.Drawing.Size(294, 140)
		Me.LayoutControlGroup2.Text = "CRC File header information"
		Me.LayoutControlGroup2.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
		'
		'LayoutControlItem3
		'
		Me.LayoutControlItem3.Control = Me.TxtDataOriginator
		Me.LayoutControlItem3.CustomizationFormText = "Data Originator"
		Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 48)
		Me.LayoutControlItem3.Name = "LayoutControlItem3"
		Me.LayoutControlItem3.Size = New System.Drawing.Size(135, 24)
		Me.LayoutControlItem3.Text = "Data Originator"
		Me.LayoutControlItem3.TextSize = New System.Drawing.Size(78, 13)
		'
		'LayoutControlItem1
		'
		Me.LayoutControlItem1.Control = Me.TxtGeneratorTool
		Me.LayoutControlItem1.CustomizationFormText = "Generator Tool"
		Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
		Me.LayoutControlItem1.Name = "LayoutControlItem1"
		Me.LayoutControlItem1.Size = New System.Drawing.Size(135, 24)
		Me.LayoutControlItem1.Text = "Generator Tool"
		Me.LayoutControlItem1.TextSize = New System.Drawing.Size(78, 13)
		'
		'LayoutControlItem2
		'
		Me.LayoutControlItem2.Control = Me.TxtFileType
		Me.LayoutControlItem2.CustomizationFormText = "File Type"
		Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 24)
		Me.LayoutControlItem2.Name = "LayoutControlItem2"
		Me.LayoutControlItem2.Size = New System.Drawing.Size(135, 24)
		Me.LayoutControlItem2.Text = "File Type"
		Me.LayoutControlItem2.TextSize = New System.Drawing.Size(78, 13)
		'
		'LayoutControlItem6
		'
		Me.LayoutControlItem6.Control = Me.TxtLastModified
		Me.LayoutControlItem6.CustomizationFormText = "Last Modified"
		Me.LayoutControlItem6.Location = New System.Drawing.Point(135, 48)
		Me.LayoutControlItem6.Name = "LayoutControlItem6"
		Me.LayoutControlItem6.Size = New System.Drawing.Size(135, 24)
		Me.LayoutControlItem6.Text = "Last Modified"
		Me.LayoutControlItem6.TextSize = New System.Drawing.Size(78, 13)
		'
		'LayoutControlItem5
		'
		Me.LayoutControlItem5.Control = Me.TxtCreationDate
		Me.LayoutControlItem5.CustomizationFormText = "Creation Date"
		Me.LayoutControlItem5.Location = New System.Drawing.Point(135, 24)
		Me.LayoutControlItem5.Name = "LayoutControlItem5"
		Me.LayoutControlItem5.Size = New System.Drawing.Size(135, 24)
		Me.LayoutControlItem5.Text = "Creation Date"
		Me.LayoutControlItem5.TextSize = New System.Drawing.Size(78, 13)
		'
		'LayoutControlItem4
		'
		Me.LayoutControlItem4.Control = Me.TxtCompanyName
		Me.LayoutControlItem4.CustomizationFormText = "Company Name"
		Me.LayoutControlItem4.Location = New System.Drawing.Point(135, 0)
		Me.LayoutControlItem4.Name = "LayoutControlItem4"
		Me.LayoutControlItem4.Size = New System.Drawing.Size(135, 24)
		Me.LayoutControlItem4.Text = "Company Name"
		Me.LayoutControlItem4.TextSize = New System.Drawing.Size(78, 13)
		'
		'LayoutControlItem7
		'
		Me.LayoutControlItem7.Control = Me.TxtSourceFileName
		Me.LayoutControlItem7.CustomizationFormText = "Source Filename"
		Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 72)
		Me.LayoutControlItem7.Name = "LayoutControlItem7"
		Me.LayoutControlItem7.Size = New System.Drawing.Size(135, 24)
		Me.LayoutControlItem7.Text = "Source Filename"
		Me.LayoutControlItem7.TextSize = New System.Drawing.Size(78, 13)
		'
		'EmptySpaceItem1
		'
		Me.EmptySpaceItem1.AllowHotTrack = false
		Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
		Me.EmptySpaceItem1.Location = New System.Drawing.Point(135, 72)
		Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
		Me.EmptySpaceItem1.Size = New System.Drawing.Size(135, 24)
		Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
		'
		'gridControl
		'
		Me.gridControl.Dock = System.Windows.Forms.DockStyle.Fill
		Me.gridControl.Location = New System.Drawing.Point(0, 0)
		Me.gridControl.MainView = Me.gridView1
		Me.gridControl.Name = "gridControl"
		Me.gridControl.Size = New System.Drawing.Size(937, 445)
		Me.gridControl.TabIndex = 1
		Me.gridControl.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gridView1})
		'
		'gridView1
		'
		Me.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.Transparent
		Me.gridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.Black
		Me.gridView1.Appearance.FocusedRow.Options.UseBackColor = true
		Me.gridView1.Appearance.FocusedRow.Options.UseBorderColor = true
		Me.gridView1.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8!, System.Drawing.FontStyle.Bold)
		Me.gridView1.Appearance.HeaderPanel.Options.UseFont = true
		Me.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true
		Me.gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
		Me.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
		Me.gridView1.FormatConditions.AddRange(New DevExpress.XtraGrid.StyleFormatCondition() {StyleFormatCondition1})
		Me.gridView1.GridControl = Me.gridControl
		Me.gridView1.Name = "gridView1"
		Me.gridView1.OptionsBehavior.ReadOnly = true
		Me.gridView1.OptionsCustomization.AllowColumnMoving = false
		Me.gridView1.OptionsCustomization.AllowQuickHideColumns = false
		Me.gridView1.OptionsCustomization.AllowRowSizing = true
		Me.gridView1.OptionsDetail.EnableDetailToolTip = true
		Me.gridView1.OptionsMenu.EnableColumnMenu = false
		Me.gridView1.OptionsMenu.EnableFooterMenu = false
		Me.gridView1.OptionsMenu.EnableGroupPanelMenu = false
		Me.gridView1.OptionsMenu.ShowAutoFilterRowItem = false
		Me.gridView1.OptionsMenu.ShowDateTimeGroupIntervalItems = false
		Me.gridView1.OptionsMenu.ShowGroupSortSummaryItems = false
		Me.gridView1.OptionsView.BestFitMaxRowCount = 15
		Me.gridView1.OptionsView.ColumnAutoWidth = false
		Me.gridView1.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.Hidden
		Me.gridView1.OptionsView.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow
		Me.gridView1.OptionsView.ShowGroupExpandCollapseButtons = false
		Me.gridView1.OptionsView.ShowGroupPanel = false
		'
		'popupControlContainer2
		'
		Me.popupControlContainer2.Appearance.BackColor = System.Drawing.Color.Transparent
		Me.popupControlContainer2.Appearance.Options.UseBackColor = true
		Me.popupControlContainer2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
		Me.popupControlContainer2.Controls.Add(Me.buttonEdit)
		Me.popupControlContainer2.Location = New System.Drawing.Point(238, 289)
		Me.popupControlContainer2.Name = "popupControlContainer2"
		Me.popupControlContainer2.Ribbon = Me.ribbonControl
		Me.popupControlContainer2.Size = New System.Drawing.Size(118, 28)
		Me.popupControlContainer2.TabIndex = 7
		Me.popupControlContainer2.Visible = false
		'
		'buttonEdit
		'
		Me.buttonEdit.EditValue = "Some Text"
		Me.buttonEdit.Location = New System.Drawing.Point(3, 5)
		Me.buttonEdit.MenuManager = Me.ribbonControl
		Me.buttonEdit.Name = "buttonEdit"
		Me.buttonEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
		Me.buttonEdit.Size = New System.Drawing.Size(100, 20)
		Me.buttonEdit.TabIndex = 0
		'
		'popupControlContainer1
		'
		Me.popupControlContainer1.Appearance.BackColor = System.Drawing.Color.Transparent
		Me.popupControlContainer1.Appearance.Options.UseBackColor = true
		Me.popupControlContainer1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
		Me.popupControlContainer1.Controls.Add(Me.someLabelControl2)
		Me.popupControlContainer1.Controls.Add(Me.someLabelControl1)
		Me.popupControlContainer1.Location = New System.Drawing.Point(111, 197)
		Me.popupControlContainer1.LookAndFeel.UseDefaultLookAndFeel = false
		Me.popupControlContainer1.Name = "popupControlContainer1"
		Me.popupControlContainer1.Ribbon = Me.ribbonControl
		Me.popupControlContainer1.Size = New System.Drawing.Size(76, 70)
		Me.popupControlContainer1.TabIndex = 6
		Me.popupControlContainer1.Visible = false
		'
		'someLabelControl2
		'
		Me.someLabelControl2.Location = New System.Drawing.Point(3, 57)
		Me.someLabelControl2.Name = "someLabelControl2"
		Me.someLabelControl2.Size = New System.Drawing.Size(49, 13)
		Me.someLabelControl2.TabIndex = 0
		Me.someLabelControl2.Text = "Some Info"
		'
		'someLabelControl1
		'
		Me.someLabelControl1.Location = New System.Drawing.Point(3, 3)
		Me.someLabelControl1.Name = "someLabelControl1"
		Me.someLabelControl1.Size = New System.Drawing.Size(49, 13)
		Me.someLabelControl1.TabIndex = 0
		Me.someLabelControl1.Text = "Some Info"
		'
		'OpenFileDialog1
		'
		Me.OpenFileDialog1.FileName = "OpenFileDialog1"
		'
		'AlertControl1
		'
		Me.AlertControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
		Me.AlertControl1.AppearanceCaption.Options.UseFont = true
		Me.AlertControl1.AutoHeight = true
		Me.AlertControl1.Images = Me.ribbonImageCollectionLarge
		Me.AlertControl1.ShowPinButton = false
		'
		'DefaultLookAndFeel1
		'
		Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "Sharp"
		'
		'FrmMain
		'
		Me.AllowFormGlass = DevExpress.Utils.DefaultBoolean.[False]
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(953, 655)
		Me.Controls.Add(Me.splitContainerControl)
		Me.Controls.Add(Me.popupControlContainer1)
		Me.Controls.Add(Me.popupControlContainer2)
		Me.Controls.Add(Me.ribbonStatusBar)
		Me.Controls.Add(Me.ribbonControl)
		Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
		Me.Name = "FrmMain"
		Me.Ribbon = Me.ribbonControl
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.StatusBar = Me.ribbonStatusBar
		Me.Text = "United ATS CRC Creator"
		CType(Me.RepositoryItemPictureEdit4,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.splitContainerControl,System.ComponentModel.ISupportInitialize).EndInit
		Me.splitContainerControl.ResumeLayout(false)
		CType(Me.LayoutControl1,System.ComponentModel.ISupportInitialize).EndInit
		Me.LayoutControl1.ResumeLayout(false)
		CType(Me.TxtSourceFileName.Properties,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.ribbonControl,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.appMenu,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.ribbonImageCollection,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.RepositoryItemProgressBar1,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.RepositoryItemPictureEdit3,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.RepositoryItemImageEdit3,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.ribbonImageCollectionLarge,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.RepositoryItemImageEdit1,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.RepositoryItemImageEdit2,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.RepositoryItemPictureEdit1,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.RepositoryItemPictureEdit2,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.TxtLastModified.Properties,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.TxtCreationDate.Properties,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.TxtCompanyName.Properties,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.TxtDataOriginator.Properties,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.TxtGeneratorTool.Properties,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.TxtFileType.Properties,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.LayoutControlGroup1,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.LayoutControlGroup2,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.LayoutControlItem3,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.LayoutControlItem1,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.LayoutControlItem2,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.LayoutControlItem6,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.LayoutControlItem5,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.LayoutControlItem4,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.LayoutControlItem7,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.EmptySpaceItem1,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.gridControl,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.gridView1,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.popupControlContainer2,System.ComponentModel.ISupportInitialize).EndInit
		Me.popupControlContainer2.ResumeLayout(false)
		CType(Me.buttonEdit.Properties,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.popupControlContainer1,System.ComponentModel.ISupportInitialize).EndInit
		Me.popupControlContainer1.ResumeLayout(false)
		Me.popupControlContainer1.PerformLayout
		Me.ResumeLayout(false)
		Me.PerformLayout

End Sub
    Private WithEvents splitContainerControl As DevExpress.XtraEditors.SplitContainerControl
    Private WithEvents ribbonControl As DevExpress.XtraBars.Ribbon.RibbonControl
    Private WithEvents siStatus As DevExpress.XtraBars.BarStaticItem
    Private WithEvents siCrcTotalRecords As DevExpress.XtraBars.BarStaticItem
    Private WithEvents homeRibbonPage As DevExpress.XtraBars.Ribbon.RibbonPage
    Private WithEvents fileRibbonPageGroup As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Private WithEvents iOpen As DevExpress.XtraBars.BarButtonItem
    Private WithEvents iClose As DevExpress.XtraBars.BarButtonItem
    Private WithEvents alignButtonGroup As DevExpress.XtraBars.BarButtonGroup
    Private WithEvents iBoldFontStyle As DevExpress.XtraBars.BarButtonItem
    Private WithEvents iItalicFontStyle As DevExpress.XtraBars.BarButtonItem
    Private WithEvents iUnderlinedFontStyle As DevExpress.XtraBars.BarButtonItem
    Private WithEvents fontStyleButtonGroup As DevExpress.XtraBars.BarButtonGroup
    Private WithEvents iLeftTextAlign As DevExpress.XtraBars.BarButtonItem
    Private WithEvents iCenterTextAlign As DevExpress.XtraBars.BarButtonItem
    Private WithEvents iRightTextAlign As DevExpress.XtraBars.BarButtonItem
    Private WithEvents rgbiSkins As DevExpress.XtraBars.RibbonGalleryBarItem
    Private WithEvents exitRibbonPageGroup As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Private WithEvents iExit As DevExpress.XtraBars.BarButtonItem
    Private WithEvents helpRibbonPage As DevExpress.XtraBars.Ribbon.RibbonPage
    Private WithEvents helpRibbonPageGroup As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Private WithEvents iHelp As DevExpress.XtraBars.BarButtonItem
    Private WithEvents iAbout As DevExpress.XtraBars.BarButtonItem
    Private WithEvents appMenu As DevExpress.XtraBars.Ribbon.ApplicationMenu
    Private WithEvents popupControlContainer1 As DevExpress.XtraBars.PopupControlContainer
    Private WithEvents someLabelControl2 As DevExpress.XtraEditors.LabelControl
    Private WithEvents someLabelControl1 As DevExpress.XtraEditors.LabelControl
    Private WithEvents popupControlContainer2 As DevExpress.XtraBars.PopupControlContainer
    Private WithEvents buttonEdit As DevExpress.XtraEditors.ButtonEdit
    Private WithEvents ribbonStatusBar As DevExpress.XtraBars.Ribbon.RibbonStatusBar
    Private WithEvents ribbonImageCollection As DevExpress.Utils.ImageCollection
    Private WithEvents ribbonImageCollectionLarge As DevExpress.Utils.ImageCollection
    Private WithEvents gridControl As DevExpress.XtraGrid.GridControl
    Private WithEvents gridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents ToolTipController1 As DevExpress.Utils.ToolTipController
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents TxtLastModified As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtCreationDate As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtCompanyName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtDataOriginator As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtGeneratorTool As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtFileType As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents TxtSourceFileName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents siCrcValidRecords As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents siCrcInvalidRecords As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents RepositoryItemImageEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemImageEdit
    Friend WithEvents AlertControl1 As DevExpress.XtraBars.Alerter.AlertControl
    Friend WithEvents crcValidationProgress As DevExpress.XtraBars.BarEditItem
    Friend WithEvents RepositoryItemProgressBar1 As DevExpress.XtraEditors.Repository.RepositoryItemProgressBar
    Friend WithEvents RepositoryItemPictureEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit
    Friend WithEvents RepositoryItemImageEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemImageEdit
    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents RepositoryItemPictureEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit
    Friend WithEvents BarEditItem2 As DevExpress.XtraBars.BarEditItem
    Friend WithEvents RepositoryItemPictureEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit
    Friend WithEvents BarEditItem3 As DevExpress.XtraBars.BarEditItem
    Friend WithEvents RepositoryItemImageEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemImageEdit
    Friend WithEvents BarEditItem1 As DevExpress.XtraBars.BarEditItem
    Friend WithEvents RepositoryItemPictureEdit4 As DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit
    Friend WithEvents iReRead As BarButtonItem
	Friend WithEvents iGenerateCrc As BarButtonItem
	Friend WithEvents RibbonPageGroupGenerateCrc As RibbonPageGroup
	Friend WithEvents iExportCrc As BarButtonItem
	Friend WithEvents iExportCsv As BarButtonItem
End Class
