﻿Imports System.Text

Friend Class ClsCRC32Q

    Private _dataString As String = String.Empty

    Public Sub New(ByVal vDataStr As String)
        If Not String.IsNullOrEmpty(vDataStr) Then
            _dataString = vDataStr.Replace(",", "")
        End If
    End Sub

    Public ReadOnly Property CrcValue As String
        Get
            If Not String.IsNullOrEmpty(_dataString) Then
                CrcValue = GetCRC32QHex(_dataString)
            Else
                CrcValue = "NaN"
            End If
        End Get
    End Property

    ''' <summary>
    ''' Calculate the CRC32Q value for the corrsponding string
    ''' </summary>
    Protected Function GetCRC32QHex(ByVal data As String) As String
        Dim crc As UInteger = 0
        Dim mustXOR As Boolean = False
        Dim newbit As UInteger = 0

        Dim polyQ As UInteger = &H814141ABUI


        '   convert the string to bytes. Use the appropriate Encoding
        Dim bytes As New List(Of Byte)(Encoding.UTF8.GetBytes(data))
        '   must add 4 'empty' bytes to the end
        bytes.AddRange(New Byte() {0, 0, 0, 0})


        For indx As Integer = 0 To bytes.Count - 1
            For bit = 7 To 0 Step -1
                '   need to XOR with poly if most significant bit of crc is set....
                mustXOR = (crc And &H80000000UI) <> 0

                '   .... but first, must shift crc 1 bit to left, losing top bit
                '   while shifting in next bit from data byte as bottom bit of crc
                If (bytes(indx) And (1 << bit)) <> 0 Then newbit = 1 Else newbit = 0
                crc = (crc << 1) Or newbit

                '   now do the XOR, if needed
                If mustXOR Then crc = crc Xor polyQ
            Next bit
        Next

        Return crc.ToString("X8")
    End Function

End Class
