﻿Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.Skins
Imports DevExpress.LookAndFeel
Imports DevExpress.UserSkins
Imports DevExpress.XtraBars
Imports DevExpress.XtraBars.Ribbon
Imports DevExpress.XtraBars.Helpers
Imports DevExpress.XtraGrid
Imports System.IO
Imports System.Text.RegularExpressions
Imports DevExpress.Utils
Imports System.Globalization
Imports System.Linq
Imports System.Text
Imports DevExpress.XtraBars.Alerter
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports DevExpress.XtraGrid.Drawing
Imports DevExpress.XtraEditors
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraReports.Parameters
Imports DevExpress.XtraSplashScreen

'--------------------------------------------------------------
' Project   : United ATS CRC32 Validator
' Developer : Eng. Ahmed Samy
' E-mail    : Ahmedgis@ymail.com
' Date      : 7/5/2013
' Updated	: 20/10/2019
'--------------------------------------------------------------


Public Class FrmMain

#Region "     Variables"

	Dim _crcFileGeneratorTool As String = String.Empty
	Dim _crcFileType As String = String.Empty
	Dim _crcFileCreationDate As String = String.Empty
	Dim _crcFileDataOriginator As String = String.Empty
	Dim _crcFileCompanyName As String = String.Empty
	Dim _crcFileSourceFileName As String = String.Empty
	Dim _crcFileLastModified As String = String.Empty

	Dim _crcTagFileGeneratorTool As String = "# Generator:"
	Dim _crcTagFileType As String = "# File Type:"
	Dim _crcTagFileCreationDate As String = "# Creation Date:"
	Dim _crcTagFileDataOriginator As String = "# Data Originator:"
	Dim _crcTagFileCompanyName As String = "# Company Name:"
	Dim _crcTagFileSourceFileName As String = "# Source Filename:"
	Dim _crcTagFileLastModified As String = "# Last Modified:"

	Dim _crcFileRecordsSplitFields As List(Of String())
	Dim _crcFileRecordsCommaFields As List(Of String)

	Dim _validCrcRecordsCount As Double = 0

	' The Grid control DataTable
	Dim recordsDataTable As New DataTable

	Dim inputFileName As String = ""

	Dim _is1stRowIsHeader As Boolean = False

	Dim CRC_FIELD_NAME As String = "CRC"

	Dim IsReadFileActive As Boolean = False
	Dim IsGenerateCRCFileActive As Boolean = False

#End Region

	Sub New()
		InitSkins()
		InitializeComponent()
		Me.InitSkinGallery()
		'Me.InitTreeListControl()
		'Me.InitGrid()

		'LoadCrcFileFieldsInGrid(gridControl)

	End Sub
	Sub InitSkins()
		DevExpress.Skins.SkinManager.EnableFormSkins()
		DevExpress.UserSkins.BonusSkins.Register()
		UserLookAndFeel.Default.SetSkinStyle("DevExpress Style")

	End Sub
	Private Sub InitSkinGallery()
		SkinHelper.InitSkinGallery(rgbiSkins, True)
	End Sub



#Region "     Load CRC File Fieldsto the grid"

	Private Function ReadCcrFile(ByVal vFileName As String, ByRef crcFileRecordsSplitFields As List(Of String()), ByRef crcFileRecordsCommaFields As List(Of String)) As Integer
		' Read CRC File
		' Return Fields Count
		Dim inline As String = String.Empty

		If FileSystem.FileLen(vFileName) <= 0 Then Return -1 ' means no fields found

		If File.Exists(vFileName) Then
			Dim infile As StreamReader = New StreamReader(vFileName)

			If infile.EndOfStream = False Then
				While (infile.EndOfStream = False)

					inline = infile.ReadLine()

					If Not String.IsNullOrEmpty(inline) Then

						'	Check for the wrong line end, remove the last comma - if any -
						If inline.EndsWith(",") Then
							inline = inline.Substring(0, inline.Length - 1)
						End If

						If inline.StartsWith("#") Then
							' Parse File header
							ParseCcrFileHeaderLine(inline)

						Else
							' parse file records, put them in a collection of string array [from Split fn]
							ParseCcrFileRecordLine(inline, crcFileRecordsSplitFields, crcFileRecordsCommaFields)

						End If
					End If
				End While


				infile.Close()
				infile.Dispose()

				Return 1

			End If
		Else
			XtraMessageBox.Show("File not found in the selected path" & vbNewLine & vFileName, "Error Opening file", MessageBoxButtons.OK, MessageBoxIcon.Error)
		End If



	End Function

	Private Sub ParseCcrFileHeaderLine(ByVal headerlineStr As String)
		' Parse the CRC File Header Line per Line to extract the CRC file Header information

		If headerlineStr.StartsWith(_crcTagFileGeneratorTool, True, CultureInfo.CreateSpecificCulture("en-us")) Then
			_crcFileGeneratorTool = headerlineStr.Substring(_crcTagFileGeneratorTool.Length, headerlineStr.Length - _crcTagFileGeneratorTool.Length).Trim()
		ElseIf headerlineStr.StartsWith(_crcTagFileType, True, CultureInfo.CreateSpecificCulture("en-us")) Then
			_crcFileType = headerlineStr.Substring(_crcTagFileType.Length, headerlineStr.Length - _crcTagFileType.Length).Trim()
		ElseIf headerlineStr.StartsWith(_crcTagFileCreationDate, True, CultureInfo.CreateSpecificCulture("en-us")) Then
			_crcFileCreationDate = headerlineStr.Substring(_crcTagFileCreationDate.Length, headerlineStr.Length - _crcTagFileCreationDate.Length).Trim()
		ElseIf headerlineStr.StartsWith(_crcTagFileDataOriginator, True, CultureInfo.CreateSpecificCulture("en-us")) Then
			_crcFileDataOriginator = headerlineStr.Substring(_crcTagFileDataOriginator.Length, headerlineStr.Length - _crcTagFileDataOriginator.Length).Trim()
		ElseIf headerlineStr.StartsWith(_crcTagFileCompanyName, True, CultureInfo.CreateSpecificCulture("en-us")) Then
			_crcFileCompanyName = headerlineStr.Substring(_crcTagFileCompanyName.Length, headerlineStr.Length - _crcTagFileCompanyName.Length).Trim()
		ElseIf headerlineStr.StartsWith(_crcTagFileSourceFileName, True, CultureInfo.CreateSpecificCulture("en-us")) Then
			_crcFileSourceFileName = headerlineStr.Substring(_crcTagFileSourceFileName.Length, headerlineStr.Length - _crcTagFileSourceFileName.Length).Trim()
		ElseIf headerlineStr.StartsWith(_crcTagFileLastModified, True, CultureInfo.CreateSpecificCulture("en-us")) Then
			_crcFileLastModified = headerlineStr.Substring(_crcTagFileLastModified.Length, headerlineStr.Length - _crcTagFileLastModified.Length).Trim()
		End If
	End Sub

	Private Function ParseCcrFileHeaderRow(ByVal headerRowStr As String) As List(Of String)
		' Parse the CRC File Header row to extract the CRC file Header columns

		Dim headerColumns() As String = {}

		If Not String.IsNullOrEmpty(headerRowStr) Then
			headerColumns = headerRowStr.Split(",")
		End If

		Return headerColumns.ToList()

	End Function

	Private Sub ParseCcrFileRecordLine(ByVal recordlineStr As String, ByRef crcFileRecordsSplitFields As List(Of String()), ByRef crcFileRecordsCommaFields As List(Of String))


		Dim recordFldsValArr() = recordlineStr.Replace("""", "").Split(",")

		If recordFldsValArr.Length > 0 Then

			' Add the Record Fields Value Array to the Collection

			'_crcFileRecordsSplitFields.Add(recordFldsValArr)
			'_crcFileRecordsCommaFields.Add(recordlineStr.Replace("""", ""))

			crcFileRecordsSplitFields.Add(recordFldsValArr)
			crcFileRecordsCommaFields.Add(recordlineStr.Replace("""", ""))

		End If


	End Sub
	Private Function SelectOnlyCrcFieldValue(ByVal recordlineStrSplitted() As String) As String
		' Take the splitted record string and strip the last array element as the CRC32 field value to be check later
		Try
			Return recordlineStrSplitted(recordlineStrSplitted.Length - 1).ToUpper()
		Catch ex As System.Exception
			Return "00000000"
		End Try
	End Function
	Private Function SelectOnlyCrcFieldValue(ByVal recordlineStrComma As String) As String
		' Take the splitted record string and strip the last array element as the CRC32 field value to be check later
		Try
			Dim lastCommaIndex As Integer = recordlineStrComma.LastIndexOf(",")

			If lastCommaIndex > 0 Then
				Return recordlineStrComma.Substring(lastCommaIndex + 1, recordlineStrComma.Length - lastCommaIndex - 1).ToUpper()
			Else
				Return ""
			End If
		Catch ex As System.Exception
			Return "00000000"
		End Try

	End Function

	''' <summary>
	''' Get the CRC field index from the string row
	''' </summary>
	''' <param name="recordlineStrComma"></param>
	''' <returns></returns>
	Private Function GetCrcFieldIndex(ByVal recordlineStrComma As String) As Integer

		'	Find the CRC field position\index -if any-
		Try

			Dim recordLineStrArr() As String = recordlineStrComma.Split(",")

			If recordLineStrArr.Length > 0 Then

				'	Check if the last field matching the CRC32 criteria or not
				If IsValidCrcFldType(recordLineStrArr(recordLineStrArr.Length - 1)) Then
					'	CRC field NOT existed, return the array length -1 with zero index order

					Return recordLineStrArr.Length - 1
				Else
					Return -1
				End If

				'If recordLineStrArr.Contains(CRC_FIELD_NAME) Then
				'	'	CRC field existed
				'	Return Array.IndexOf(recordLineStrArr, CRC_FIELD_NAME)
				'Else
				'	'	CRC field NOT existed, return the array length -1 with zero index order

				'	Return recordLineStrArr.Length - 1
				'End If

			End If

		Catch ex As System.Exception
			Return -1
		End Try


	End Function
	Private Function ExcludeCrcFieldValue(ByVal recordlineStrComma As String) As String
		' Remove the splitted record string and return the string without it
		Try
			Dim lastCommaIndex As Integer = recordlineStrComma.LastIndexOf(",")

			If lastCommaIndex > 0 Then
				Return recordlineStrComma.Substring(0, lastCommaIndex)
			Else
				Return ""
			End If
		Catch ex As System.Exception
			Return ""
		End Try

	End Function
	Private Function IsValidCrcFldType(ByVal crcFldValue As String) As Boolean
		' Check if the Last field of the Obstacle or Aerodrome facility is a CRC Field or not

		' Check the CRC Field Value Conditions: [Using Regular expression]
		' 1- Length = 8 Char
		' 2- ASCII Letters (A B C D E F)

		Dim foundMatch As Boolean = Regex.IsMatch(crcFldValue, "[A-Fa-f0-9]{8}", RegexOptions.IgnoreCase)

		If foundMatch Then
			Return True
		Else
			Return False
		End If

	End Function

	Private Function CalculateCRCValue(ByVal recordStr As String) As String
		Dim clsCrc32 As New ClsCRC32Q(recordStr)

		Return clsCrc32.CrcValue.ToUpper()

	End Function

	Private Sub AddFileRecordsToGridAndCheckCrc(ByRef vGridControl As GridControl, ByRef crcFileRecordsSplitFields As List(Of String()), ByRef crcFileRecordsCommaFields As List(Of String))

		'Reset Valid CRC Records counter
		_validCrcRecordsCount = 0


		RepositoryItemProgressBar1.Step = 1
		RepositoryItemProgressBar1.PercentView = True
		RepositoryItemProgressBar1.Maximum = crcFileRecordsSplitFields.Count
		RepositoryItemProgressBar1.Minimum = 0
		crcValidationProgress.EditValue = RepositoryItemProgressBar1.Minimum
		crcValidationProgress.Width = 150

		Dim recordCrcValueStr As String = "NULL"

		Dim index As Integer = 0

		For Each recordStr As String In crcFileRecordsCommaFields.ToList()
			'Dim recordStrWithoutCrcFld As String = ExcludeCrcFieldValue(recordStr)

			'Dim givenCrcRecordValue As String = SelectOnlyCrcFieldValue(recordStr)' Check if the Given CRC Field Value is Valid CRC value or the record didn't ends with a CRC field value
			'Dim recordStatusStr As String = "Valid CRC"
			'Dim recordStatusStr As String = "NO CRC"


			''If Not IsValidCrcFldType(givenCrcRecordValue) Then
			''	recordStatusStr = "CRC Field not found"
			''Else
			''	' Calculate the CRC32 value for the comma record string
			''	Dim calculatedCrcRecordValue As String = CalculateCRCValue(recordStrWithoutCrcFld)

			''	' Compaire the (Given, Calculated) CRC values
			''	Dim isValidGivenCrcValue As Boolean = (givenCrcRecordValue = calculatedCrcRecordValue)

			''	If Not isValidGivenCrcValue Then recordStatusStr = "Invalid CRC"

			''End If

			''If recordStatusStr = "Valid CRC" Then _validCrcRecordsCount += 1



			'	Get the CRC field poisition with zero index order
			Dim crcFieldIndex As Integer = GetCrcFieldIndex(recordStr)


			''	If CRC Field is already existed
			'If crcFieldIndex > -1 Then

			'	'	Exclude the CRC field value from the comma string
			'	recordStr = ExcludeCrcFieldValue(recordStr)
			'End If

			' Add the record to the Grid control
			'AddRowToDataTable(recordStr, recordsDataTable, recordCrcValueStr, recordStatusStr)
			AddRowToDataTable(recordStr, recordsDataTable, recordCrcValueStr, crcFieldIndex, False)
			''--''crcFileRecordsCommaFields(index) = AddRowToDataTable(recordStr, recordsDataTable, recordCrcValueStr, crcFieldIndex, False)


			'Dim rowStrFldsValArr() As String = {}

			'If crcFieldIndex = -1 Then

			'ElseIf crcFieldIndex > -1 Then
			'	rowStrFldsValArr = recordStr.Split(",")


			'	'	ONLY update the CRC field value if this function was called from GenerateCRC Function, NOT the ReadCRC Function
			'	If isOnlyGenerateCrcFile Then

			'		recordStr = String.Format("{0},{1}", recordStr,recordCrcValueStr)

			'		rowStrFldsValArr = recordStr.Split(",")

			'		'	Set the CRC field value
			'		''rowStrFldsValArr.SetValue(crcFieldValue, crcFieldIndex)
			'	Else
			'		'	if We ONLY read the record value
			'		'	Nothing to doEnd If
			'	End If
			'End If


			index += 1
			crcValidationProgress.EditValue += 1
			Application.DoEvents()

		Next

		' Strip CRC Field and check its validity
	End Sub

	Private Sub AddCrcValueforFileRecordsToGrid(ByRef vGridControl As GridControl, ByRef crcFileRecordsSplitFields As List(Of String()), ByRef crcFileRecordsCommaFields As List(Of String))

		'Reset Valid CRC Records counter
		_validCrcRecordsCount = 0

		RepositoryItemProgressBar1.Step = 1
		RepositoryItemProgressBar1.PercentView = True
		RepositoryItemProgressBar1.Maximum = crcFileRecordsSplitFields.Count
		RepositoryItemProgressBar1.Minimum = 0
		crcValidationProgress.EditValue = RepositoryItemProgressBar1.Minimum
		crcValidationProgress.Width = 150

		'Dim status As String = ""
		Dim calculatedCrcRecordValue As String = ""
		Dim index As Integer = 0

		For Each recordStr As String In crcFileRecordsCommaFields.ToList()


			Try

				'	Get the CRC field position - if any -
				Dim crcFieldIndex As Integer = GetCrcFieldIndex(recordStr)

				'	If CRC Field is already existed
				If crcFieldIndex > -1 Then

					'	Exclude the OLD CRC field value from the comma string
					recordStr = ExcludeCrcFieldValue(recordStr)
				End If


				' Calculate the CRC32 value for the comma record string
				calculatedCrcRecordValue = CalculateCRCValue(recordStr)

				'	Add the new calculated CRC value to the comma string
				''--''recordStr = String.Format("{0},{1}", recordStr,calculatedCrcRecordValue)


				' Add the record to the Grid control
				'AddRowToDataTable(recordStr, recordsDataTable, calculatedCrcRecordValue, status)
				crcFileRecordsCommaFields(index) = AddRowToDataTable(recordStr, recordsDataTable, calculatedCrcRecordValue, crcFieldIndex, True)

				_validCrcRecordsCount += 1

				crcValidationProgress.EditValue += 1
				Application.DoEvents()

			Catch ex As Exception
				'status = "CRC Not Created"
				AlertControl1.Show(Me, "Add CRC field", ex.Message)
			End Try

		Next

		' Strip CRC Field and check its validity
	End Sub

	Private Function IsAllUpperCase(ByVal value As String) As Boolean
		Return (value.Equals(value.ToUpper()))
	End Function

	Private Function IsHeaderRow(ByVal rowComma As String) As Boolean
		If Not String.IsNullOrEmpty(rowComma) Then
			Dim rowCommaArr() As String = rowComma.Split(",")

			If rowCommaArr.Length > 0 Then
				For Each fld As String In rowCommaArr
					If IsNumeric(fld) OrElse String.IsNullOrEmpty(fld) Then
						Return False
					End If
				Next

				'	If NO field value is numeric, so this might be a header row
				Return True
			Else
				Return False
			End If
		Else
			Return False
		End If
	End Function

	Private Function GetMaxFldsCount(ByRef crcFileRecordsSplitFields As List(Of String())) As Integer
		' Get the max crc file record fields count to build the table acc.to the maximum

		Dim maxFldCount As Integer = 0

		For i = 0 To crcFileRecordsSplitFields.Count - 1
			If crcFileRecordsSplitFields(i).Length > maxFldCount Then
				maxFldCount = crcFileRecordsSplitFields(i).Length
			End If
		Next

		Return maxFldCount
	End Function
	Private Function CreateCustomGridColsHeader(ByVal colFldNamesSplitted() As String, ByVal maxFldsCountr As Integer, ByRef errorMsg As String, Optional ByVal isOnlyGenerateCrcFile As Boolean = False) As DataTable
		Dim tbl As New DataTable()

		For j As Integer = 0 To colFldNamesSplitted.Length - 1

			Try
				tbl.Columns.Add(colFldNamesSplitted(j), GetType(String))
			Catch ex As Exception
				'	Exception will occure if the column name is existed before
				'	so we add an index to that word
				'Dim wordIndex As String = colFldNamesSplitted(j).Substring(colFldNamesSplitted(j).Length - 1, 1)
				'If IsNumeric(wordIndex) Then
				'	tbl.Columns.Add(String.Format("{0}_{1}", colFldNamesSplitted(j), CInt(wordIndex) + 1), GetType(String))
				'else
				'	tbl.Columns.Add(String.Format("{0}_1", colFldNamesSplitted(j)), GetType(String))
				'End If

				errorMsg = ex.Message

				Return Nothing

			End Try
		Next

		For i As Integer = 0 To maxFldsCountr - colFldNamesSplitted.Length - 1
			If i <= 25 Then         '  A,B,C,D,F, ..., Z
				tbl.Columns.Add(Chr(i + 65), GetType(String))
			ElseIf i > 25 Then      '  A1, A2, A3,... A(n)
				tbl.Columns.Add("A" & i - 25, GetType(String))
			End If

		Next i



		'	Check if the LAST field is an OLD CRC Value, So skip adding it

		If IsValidCrcFldType(colFldNamesSplitted(colFldNamesSplitted.Length - 1)) Then
			tbl.Columns(tbl.Columns.Count - 1).ColumnName = CRC_FIELD_NAME
		End If



		'	Only make this CRC Field check ONLY when generating the CRC file
		If isOnlyGenerateCrcFile Then

			'	check if CRC FIELD name is existed before
			If tbl.Columns.Contains(CRC_FIELD_NAME) Then

				''''Dim diagResult As DialogResult =
				''''	MessageBox.Show("This file already contains a CRC field, do you want to update its value?" & vbNewLine & vbNewLine _
				''''				& "    (YES) Update this CRC field with a new value" & vbNewLine _
				''''				& "     (NO) Create new CRC field with a new name" & vbNewLine _
				''''				& " (CANCEL) Abort the current operation and close the file",
				''''					Application.ProductName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)

				''''If diagResult = DialogResult.Yes Then
				''''	'	Skip adding the CRC field because it's already existed
				''''	'	Nothing to do here

				''''ElseIf diagResult = DialogResult.No Then
				''''	' Add an extra column for The record's CRC value
				''''	tbl.Columns.Add(String.Format("_{0}_", CRC_FIELD_NAME), GetType(String))

				''''	'	Update CRC field name
				''''	CRC_FIELD_NAME = String.Format("_{0}_", CRC_FIELD_NAME)
				''''ElseIf diagResult = DialogResult.Cancel Then
				''''	AlertControl1.Show(Me, "Create Table Header", String.Format("Canceled by user"))
				''''	XtraMessageBox.Show("The current operation is canceled by user", "Create Table Header", MessageBoxButtons.OK, MessageBoxIcon.Error)

				''''	Return Nothing
				''''End If

				'' Add an extra column for The record's CRC value
				'tbl.Columns.Add(String.Format("_{0}_", CRC_FIELD_NAME), GetType(String))

				''	Update CRC field name
				'CRC_FIELD_NAME = String.Format("_{0}_", CRC_FIELD_NAME)
			Else
				' Add an extra column for The record's CRC value
				tbl.Columns.Add(CRC_FIELD_NAME, GetType(String))
			End If


		Else
			'	if ONLY read the file

			'	check if CRC FIELD name is existed before
			If Not tbl.Columns.Contains(CRC_FIELD_NAME) Then
				' Add an extra column for The record's CRC value
				tbl.Columns.Add(CRC_FIELD_NAME, GetType(String))
			End If
		End If


		' Add an extra column for The record's CRC status
		'tbl.Columns.Add("Status", GetType(String))

		Return tbl
	End Function
	Private Function CreateRandomGridColsHeader(ByVal colFldNamesSplitted() As String, ByVal colCount As Integer) As DataTable
		Dim tbl As New DataTable()

		For i As Integer = 0 To colCount - 1
			If i <= 25 Then         '  A,B,C,D,F, ..., Z
				tbl.Columns.Add(Chr(i + 65), GetType(String))
			ElseIf i > 25 Then      '  A1, A2, A3,... A(n)
				tbl.Columns.Add("A" & i - 25, GetType(String))
			End If

		Next i



		'	Check if the LAST field is an OLD CRC Value, So skip adding it

		If IsValidCrcFldType(colFldNamesSplitted(colFldNamesSplitted.Length - 1)) Then
			tbl.Columns(tbl.Columns.Count - 1).ColumnName = CRC_FIELD_NAME
		End If


		'	check if CRC FIELD name is existed before
		If tbl.Columns.Contains(CRC_FIELD_NAME) Then

			'Dim diagResult As DialogResult =
			'MessageBox.Show("This file already contains a CRC field, do you want to update its value?" & vbNewLine & vbNewLine _
			'				& "    (YES) Update this CRC field with a new value" & vbNewLine _
			'				& "     (NO) Create new CRC field with a new name" & vbNewLine _
			'				& " (CANCEL) Abort the current operation and close the file",
			'				Application.ProductName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)

			'If diagResult = DialogResult.Yes Then
			'	Skip adding the CRC field because it's already existed
			'	Nothing to do here

			'ElseIf diagResult = DialogResult.No Then
			' Add an extra column for The record's CRC value
			'tbl.Columns.Add(String.Format("_{0}_", CRC_FIELD_NAME), GetType(String))

			'	Update CRC field name
			'CRC_FIELD_NAME = String.Format("_{0}_", CRC_FIELD_NAME)
			'ElseIf diagResult = DialogResult.Cancel Then
			'	AlertControl1.Show(Me, "Create Table Header", String.Format("Canceled by user"))
			'	XtraMessageBox.Show("The current operation is canceled by user", "Create Table Header", MessageBoxButtons.OK, MessageBoxIcon.Error)

			'	Return Nothing
			'End If
			'' Add an extra column for The record's CRC value
			'tbl.Columns.Add(String.Format("_{0}_", CRC_FIELD_NAME), GetType(String))

			''	Update CRC field name
			'CRC_FIELD_NAME = String.Format("_{0}_", CRC_FIELD_NAME)
		Else
			' Add an extra column for The record's CRC value
			tbl.Columns.Add(CRC_FIELD_NAME, GetType(String))
		End If

		' Add an extra column for The record's CRC status
		'tbl.Columns.Add("Status", GetType(String))

		Return tbl
	End Function

	'Private Sub AddRowToDataTable(ByVal rowStrComma As String, ByRef dataTable As DataTable, ByVal crcValue As String, ByVal recordStatusStr As String)
	Private Function AddRowToDataTable(ByVal rowStrComma As String, ByRef dataTable As DataTable, ByVal crcFieldValue As String, Optional ByVal crcFieldIndex As Integer = -1, Optional ByVal isOnlyGenerateCrcFile As Boolean = False) As String

		Dim rowStrFldsValArr() As String = {}

		'If crcFieldIndex = -1 Then
		'	'	CRC Field not found, then add new field at the end of the array

		'	If rowStrComma.Length > 0 Then
		'		rowStrComma += String.Format(",{0}", crcFieldValue)
		'	Else
		'		rowStrComma += String.Format("{0}", crcFieldValue)
		'	End If

		'	rowStrFldsValArr = rowStrComma.Split(",")

		'ElseIf crcFieldIndex > -1 Then
		'	'	CRC field position is known

		'	rowStrFldsValArr = rowStrComma.Split(",")

		'	'	ONLY update the CRC field value if this function was called from GenerateCRC Function, NOT the ReadCRC Function
		'	if isOnlyGenerateCrcFile Then
		'		'	Set the CRC field value
		'		rowStrFldsValArr.SetValue(crcFieldValue, crcFieldIndex)
		'	End If

		'End If

		If crcFieldIndex = -1 Then

			If rowStrComma.EndsWith(",") Then
				'	CRC Field not found, then add new field at the end of the array
				rowStrComma += String.Format("{0}", crcFieldValue)
			Else
				'	CRC Field not found, then add new field at the end of the array
				rowStrComma += String.Format(",{0}", crcFieldValue)
			End If


			rowStrFldsValArr = rowStrComma.Split(",")
		ElseIf crcFieldIndex > -1 Then

			rowStrFldsValArr = rowStrComma.Split(",")


			'	ONLY update the CRC field value if this function was called from GenerateCRC Function, NOT the ReadCRC Function
			If isOnlyGenerateCrcFile Then

				rowStrComma = String.Format("{0},{1}", rowStrComma, crcFieldValue)

				rowStrFldsValArr = rowStrComma.Split(",")

				'	Set the CRC field value
				''rowStrFldsValArr.SetValue(crcFieldValue, crcFieldIndex)
			Else
				'	if We ONLY read the record value
				'	Nothing to do
				'stop

			End If
		End If




		'	Add the row to table
		Try
			dataTable.Rows.Add(rowStrFldsValArr)

			Return rowStrComma
		Catch ex As Exception
			AlertControl1.Show(Me, "Add row to grid", ex.Message)
			Return ""
		End Try

	End Function



#End Region


#Region "       Read File       "
	Public Function ReadFile(ByVal fileName As String, ByRef crcFileRecordsSplitFields As List(Of String()), ByRef crcFileRecordsCommaFields As List(Of String)) As String

		'Dim headerColumns As New List(Of String)

		If Not String.IsNullOrEmpty(fileName) Then
			'----------------------------------------------------
			' Read the CRC file and parse all its records'----------------------------------------------------
			crcFileRecordsSplitFields = New List(Of String())()
			crcFileRecordsCommaFields = New List(Of String)()

			Call ReadCcrFile(fileName, crcFileRecordsSplitFields, crcFileRecordsCommaFields)

			'------------------------------------------------------------------------------------
			' Check the 1st Line of the file if it's the Table header or it's a record value
			' we can check this with many ways:
			'--------------------------------------
			' 1- check if the record contain any spaces, that not allowed in the crc file record. [_crcFileRecordsCommaFields(0).Contains(" ") Or _]
			' 2- check if the 1st field in the record (SiteName) more than 4 characters.
			' 3- check if all fields must be in Upper Case
			'------------------------------------------------------------------------------------
			Dim errorMsg As String = ""

			'If _crcFileRecordsCommaFields(0).Split(",")(0).Length > 4 OrElse IsAllUpperCase(_crcFileRecordsCommaFields(0)) = False Then
			'If crcFileRecordsCommaFields(0).Split(",")(0).Length > 4 OrElse IsAllUpperCase(crcFileRecordsCommaFields(0)) = False Then


			If IsHeaderRow(crcFileRecordsCommaFields(0)) Then

				'the record is the table header
				'recordsDataTable = CreateCustomGridColsHeader(_crcFileRecordsSplitFields(0), GetMaxFldsCount, errorMsg)
				recordsDataTable = CreateCustomGridColsHeader(crcFileRecordsSplitFields(0), GetMaxFldsCount(crcFileRecordsSplitFields), errorMsg)

				' Reomve the 1st item in the array that hold the crc file records
				' to avoid adding the header record again as a normal crc file record

				crcFileRecordsSplitFields.RemoveAt(0)
				crcFileRecordsCommaFields.RemoveAt(0)
			Else
				recordsDataTable = CreateRandomGridColsHeader(crcFileRecordsSplitFields(0), GetMaxFldsCount(crcFileRecordsSplitFields))
			End If
			'----------------------------------------------------

			If recordsDataTable Is Nothing Then
				'	Check if the user canceled the operation or not
				'	Check if an error happend while parsing the file header columns

				If Not String.IsNullOrEmpty(errorMsg) Then
					'	Error occured
					Return errorMsg
				Else
					'	User cancel
					Return "Operation canceled by user"
				End If
			End If

			'----------------------------------------------------
			' Show CRC File Header Information
			'----------------------------------------------------
			'TxtGeneratorTool.Text = _crcFileGeneratorTool
			'TxtFileType.Text = _crcFileType
			'TxtCreationDate.Text = _crcFileCreationDate
			'TxtDataOriginator.Text = _crcFileDataOriginator
			'TxtCompanyName.Text = _crcFileCompanyName
			'TxtSourceFileName.Text = _crcFileSourceFileName
			'TxtLastModified.Text = _crcFileLastModified
			'----------------------------------------------------

			Application.DoEvents()

			'---------------------------------------------------------------------------
			' Add all collected records to the Grid Control and check the crc values
			'---------------------------------------------------------------------------
			Call AddFileRecordsToGridAndCheckCrc(gridControl, crcFileRecordsSplitFields, crcFileRecordsCommaFields)

			'----------------------------------------------------
			' Display File details
			'----------------------------------------------------
			siStatus.Caption = "CRC File Details: "
			siCrcTotalRecords.Caption = "Total Records: " & recordsDataTable.Rows.Count
			siCrcValidRecords.Caption = "Valid CRC Records: " & _validCrcRecordsCount
			siCrcInvalidRecords.Caption = "Invalid CRC Records: " & recordsDataTable.Rows.Count - _validCrcRecordsCount
			'----------------------------------------------------


			AlertControl1.Show(Me, Application.ProductName, "File CRC validation complete" & vbNewLine & recordsDataTable.Rows.Count & "  total Records" & vbNewLine & _validCrcRecordsCount & "  records Valid CRC" & vbNewLine & recordsDataTable.Rows.Count - _validCrcRecordsCount & "  records inValid CRC")

			'ToolTipController1.ShowHint("aaahgh", siCrcValidRecords, ToolTipLocation.Default)
			'ToolTipController.DefaultController.ShowHint("aaa", "aaahgh", siCrcValidRecords, ToolTipLocation.Default, PointToScreen((New Point(0, 0))))

			gridControl.DataSource = recordsDataTable
			gridControl.RefreshDataSource()

			Application.DoEvents()

			gridView1.BestFitColumns()
			gridView1.OptionsSelection.EnableAppearanceFocusedRow = True
			gridView1.OptionsSelection.EnableAppearanceHideSelection = False
			'gridView1.OptionsView.ColumnAutoWidth = True
			gridView1.OptionsView.ShowGroupPanel = False
			gridView1.OptionsNavigation.AutoFocusNewRow = False

			gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = HorizontalAlignment.Center

			gridView1.OptionsBehavior.ReadOnly = True
			gridView1.OptionsBehavior.Editable = False

			gridView1.LoadingPanelVisible = False
			gridView1.OptionsPrint.ShowPrintExportProgress = True
			'----------------------------------------------------
			iOpen.Enabled = False
			iClose.Enabled = True
			iReRead.Enabled = True

			iGenerateCrc.Enabled = True
			iExportCrc.Enabled = False
			iExportCsv.Enabled = False

			Return ""
		ElseIf Not File.Exists(fileName) Then
			Return String.Format("The selected file: {0}{1}{1}Does not exist, please select ( Open ) File and try again", fileName, vbNewLine)
		Else
			Return String.Format("Unknown Error While Opening file:{1}{0}", fileName, vbNewLine)
		End If
	End Function
#End Region


#Region "		Generate CRC Field		"


	Private Function GenerateCrc(ByVal fileName As String, ByRef crcFileRecordsSplitFields As List(Of String()), ByRef crcFileRecordsCommaFields As List(Of String)) As String

		'Dim headerColumns As New List(Of String)

		If Not String.IsNullOrEmpty(fileName) Then
			'----------------------------------------------------
			' Read the CRC file and parse all its records
			'----------------------------------------------------
			'_crcFileRecordsSplitFields = New List(Of String())()
			'_crcFileRecordsCommaFields = New List(Of String)()

			crcFileRecordsSplitFields = New List(Of String())()
			crcFileRecordsCommaFields = New List(Of String)()

			Call ReadCcrFile(fileName, crcFileRecordsSplitFields, crcFileRecordsCommaFields)

			'------------------------------------------------------------------------------------
			' Check the 1st Line of the file if it's the Table header or it's a record value
			' we can check this with many ways:
			'--------------------------------------
			' 1- check if the record contain any spaces, that not allowed in the crc file record. [_crcFileRecordsCommaFields(0).Contains(" ") Or _]
			' 2- check if the 1st field in the record (SiteName) more than 4 characters.
			' 3- check if all fields must be in Upper Case
			'------------------------------------------------------------------------------------
			Dim errorMsg As String = ""

			'If crcFileRecordsCommaFields(0).Split(",")(0).Length > 4 OrElse IsAllUpperCase(crcFileRecordsCommaFields(0)) = False Then

			If IsHeaderRow(crcFileRecordsCommaFields(0)) Then

				'the record is the table header
				recordsDataTable = CreateCustomGridColsHeader(crcFileRecordsSplitFields(0), GetMaxFldsCount(crcFileRecordsSplitFields), errorMsg, True)

				' Reomve the 1st item in the array that hold the crc file records
				' to avoid adding the header record again as a normal crc file record

				crcFileRecordsSplitFields.RemoveAt(0)
				crcFileRecordsCommaFields.RemoveAt(0)
			Else
				recordsDataTable = CreateRandomGridColsHeader(crcFileRecordsSplitFields(0), GetMaxFldsCount(crcFileRecordsSplitFields))
			End If
			'----------------------------------------------------

			If recordsDataTable Is Nothing Then
				'	Check if the user canceled the operation or not
				'	Check if an error happend while parsing the file header columns

				If Not String.IsNullOrEmpty(errorMsg) Then
					'	Error occured
					Return errorMsg
				Else
					'	User cancel
					Return "Operation canceled by user"
				End If
			End If

			Application.DoEvents()

			'---------------------------------------------------------------------------
			' Add all collected records to the Grid Control and check the crc values
			'---------------------------------------------------------------------------
			Call AddCrcValueforFileRecordsToGrid(gridControl, crcFileRecordsSplitFields, crcFileRecordsCommaFields)

			'----------------------------------------------------
			' Display File details
			'----------------------------------------------------
			siStatus.Caption = "CRC File Details: "
			siCrcTotalRecords.Caption = "Total Records: " & recordsDataTable.Rows.Count
			siCrcValidRecords.Caption = "Generated CRC Records: " & _validCrcRecordsCount
			siCrcInvalidRecords.Caption = "Ungenerated CRC Records: " & recordsDataTable.Rows.Count - _validCrcRecordsCount
			'----------------------------------------------------


			AlertControl1.Show(Me, "Generating CRC Result", "File CRC Generation complete" & vbNewLine & recordsDataTable.Rows.Count & "  total Records" & vbNewLine & _validCrcRecordsCount & "  records Generated CRC" & vbNewLine & recordsDataTable.Rows.Count - _validCrcRecordsCount & "  records Ungenerated CRC")

			'ToolTipController1.ShowHint("aaahgh", siCrcValidRecords, ToolTipLocation.Default)
			'ToolTipController.DefaultController.ShowHint("aaa", "aaahgh", siCrcValidRecords, ToolTipLocation.Default, PointToScreen((New Point(0, 0))))

			gridControl.DataSource = recordsDataTable
			gridControl.RefreshDataSource()

			Application.DoEvents()

			gridView1.BestFitColumns()
			gridView1.OptionsSelection.EnableAppearanceFocusedRow = True
			gridView1.OptionsSelection.EnableAppearanceHideSelection = False
			'gridView1.OptionsView.ColumnAutoWidth = True
			gridView1.OptionsView.ShowGroupPanel = False
			gridView1.OptionsNavigation.AutoFocusNewRow = False

			gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = HorizontalAlignment.Center

			gridView1.OptionsBehavior.ReadOnly = True
			gridView1.OptionsBehavior.Editable = False

			'----------------------------------------------------
			iOpen.Enabled = False
			iClose.Enabled = True
			iReRead.Enabled = True

			iGenerateCrc.Enabled = True
			iExportCrc.Enabled = True
			iExportCsv.Enabled = True

			Return ""
		ElseIf Not File.Exists(fileName) Then
			Return String.Format("The selected file: {0}{1}{1}Does not exist, please select ( Open ) File and try again", fileName, vbNewLine)
		Else
			Return String.Format("Unknown Error While Opening file:{1}{0}", fileName, vbNewLine)
		End If
	End Function

#End Region

	Private Sub gridView1_RowClick(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.RowClickEventArgs) Handles gridView1.RowClick
		'ToolTipController1.ShowHint(gridView1.GetRowCellDisplayText(e.HitInfo.RowHandle, "Status"))
		ToolTipController1.ShowHint(gridView1.GetRowCellDisplayText(e.HitInfo.RowHandle, CRC_FIELD_NAME))
	End Sub


	Private Sub gridView1_RowStyle(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles gridView1.RowStyle
		If e.RowHandle > -1 Then
			'Try
			'If gridView1.GetRowCellDisplayText(e.RowHandle, "Status").ToLower() = "valid crc" OrElse
			'   gridView1.GetRowCellDisplayText(e.RowHandle, "Status").ToLower() = "crc created" Then
			'	e.Appearance.BackColor = Color.LightGreen
			'Else
			'	e.Appearance.BackColor = Color.Red
			'End If

			Try


				If IsReadFileActive AndAlso Not IsGenerateCRCFileActive Then
					'	Apply default row backcolor
					e.Appearance.BackColor = New AppearanceObject().BackColor
				ElseIf Not IsReadFileActive AndAlso IsGenerateCRCFileActive Then
					If String.IsNullOrEmpty(gridView1.GetRowCellDisplayText(e.RowHandle, CRC_FIELD_NAME)) OrElse
					   gridView1.GetRowCellDisplayText(e.RowHandle, CRC_FIELD_NAME).Equals("NULL") Then
						e.Appearance.BackColor = Color.Red
					Else
						e.Appearance.BackColor = Color.LightGreen
					End If
				End If
			Catch ex As Exception
				'	Apply default row backcolor
				e.Appearance.BackColor = New AppearanceObject().BackColor
			End Try




			'Catch ex As System.Exception
			'    XtraMessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
			'End Try


			e.Appearance.TextOptions.HAlignment = HorizontalAlignment.Center

		End If
	End Sub

	Private Sub iExit_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles iExit.ItemClick
		Dim vDialogResult As DialogResult = MessageBox.Show("Exit the program?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

		If vDialogResult = DialogResult.Yes Then
			Application.Exit()
		End If
	End Sub

	Private Sub iOpen_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles iOpen.ItemClick
		'OpenFileDialog1.Filter = "CSV Files (*.CSV)|*.csv|Comma Delimited Files (*.*)|*.*"
		OpenFileDialog1.Filter = "CSV Files (*.CSV)|*.csv"
		OpenFileDialog1.Title = "Select file to open"
		OpenFileDialog1.CheckFileExists = True
		OpenFileDialog1.AddExtension = True
		OpenFileDialog1.FileName = ""

		Dim vRes As DialogResult = OpenFileDialog1.ShowDialog()
		If vRes = DialogResult.Cancel Then Exit Sub

		'   Read the selected file

		Me.inputFileName = OpenFileDialog1.FileName

		'	Update the UI panel with the filename
		splitContainerControl.Panel2.Text = "Opened File: " & Me.inputFileName

		'	Show wait message
		Call ShowWaitFormOnTheButtomCenter("Reading file", "Please wait ...")

		IsReadFileActive = True
		IsGenerateCRCFileActive = False

		Dim result As String = ""

		Try

			result = Me.ReadFile(Me.inputFileName, Me._crcFileRecordsSplitFields, Me._crcFileRecordsCommaFields)

		Catch ex As Exception
			AlertControl1.Show(Me, "Reading CSV file", ex.Message)
			XtraMessageBox.Show(ex.Message, "Error Reading CSV file", MessageBoxButtons.OK, MessageBoxIcon.Error)
		End Try

		'IsReadFileActive = False
		'IsGenerateCRCFileActive = false

		Try

			'	Show wait message
			SplashScreenManager.CloseDefaultWaitForm()

		Catch ex As Exception

		End Try

		If Not String.IsNullOrEmpty(result) Then
			AlertControl1.Show(Me, Application.ProductName, result)
		End If

#Region "		Unused Code		"

		'      If Not String.IsNullOrEmpty(OpenFileDialog1.FileName) Then
		'	'----------------------------------------------------
		'	' Read the CRC file and parse all its records
		'	'----------------------------------------------------
		'	_crcFileRecordsSplitFields = New List(Of String())()
		'	_crcFileRecordsCommaFields = New List(Of String)()

		'	ReadCcrFile(OpenFileDialog1.FileName)

		'	'recordsDataTable = CreateGridColsHeader(_crcFileRecordsSplitFields(0).Length)

		'	'------------------------------------------------------------------------------------
		'	' Check the 1st Line of the file if it's the Table header or it's a record value
		'	' we can check this with many ways:
		'	'--------------------------------------
		'	' 1- check if the record contain any spaces, that not allowed in the crc file record. [_crcFileRecordsCommaFields(0).Contains(" ") Or _]
		'	' 2- check if the 1st field in the record (SiteName) more than 4 characters.
		'	' 3- check if all fields must be in Upper Case
		'	'------------------------------------------------------------------------------------

		'	If _crcFileRecordsCommaFields(0).Split(",")(0).Length > 4 Or
		'	   IsAllUpperCase(_crcFileRecordsCommaFields(0)) = False Then

		'		'the record is the table header
		'		recordsDataTable = CreateCustomGridColsHeader(_crcFileRecordsSplitFields(0), GetMaxFldsCount)

		'		' Reomve the 1st item in the array that hold the crc file records
		'		' to avoid adding the header record again as a normal crc file record

		'		_crcFileRecordsSplitFields.RemoveAt(0)
		'		_crcFileRecordsCommaFields.RemoveAt(0)
		'	Else
		'		recordsDataTable = CreateRandomGridColsHeader(GetMaxFldsCount)
		'	End If
		'	'----------------------------------------------------


		'	'----------------------------------------------------
		'	' Show CRC File Header Information
		'	'----------------------------------------------------
		'	TxtGeneratorTool.Text = _crcFileGeneratorTool
		'	TxtFileType.Text = _crcFileType
		'	TxtCreationDate.Text = _crcFileCreationDate
		'	TxtDataOriginator.Text = _crcFileDataOriginator
		'	TxtCompanyName.Text = _crcFileCompanyName
		'	TxtSourceFileName.Text = _crcFileSourceFileName
		'	TxtLastModified.Text = _crcFileLastModified
		'	'----------------------------------------------------

		'	Application.DoEvents()

		'	'---------------------------------------------------------------------------
		'	' Add all collected records to the Grid Control and check the crc values
		'	'---------------------------------------------------------------------------
		'	AddFileRecordsToGridAndCheckCrc(gridControl)

		'	'----------------------------------------------------
		'	' Display File details
		'	'----------------------------------------------------
		'	siStatus.Caption = "CRC File Details: "
		'	siCrcTotalRecords.Caption = "Total Records: " & recordsDataTable.Rows.Count
		'	siCrcValidRecords.Caption = "Valid CRC Records: " & _validCrcRecordsCount
		'	siCrcInvalidRecords.Caption = "Invalid CRC Records: " & recordsDataTable.Rows.Count - _validCrcRecordsCount
		'	'----------------------------------------------------


		'	AlertControl1.Show(Me, Application.ProductName, "File CRC validation complete" & vbNewLine & recordsDataTable.Rows.Count & "  total Records" & vbNewLine & _validCrcRecordsCount & "  records Valid CRC" & vbNewLine & recordsDataTable.Rows.Count - _validCrcRecordsCount & "  records inValid CRC")

		'	'ToolTipController1.ShowHint("aaahgh", siCrcValidRecords, ToolTipLocation.Default)
		'	'ToolTipController.DefaultController.ShowHint("aaa", "aaahgh", siCrcValidRecords, ToolTipLocation.Default, PointToScreen((New Point(0, 0))))

		'	gridControl.DataSource = recordsDataTable
		'	gridControl.RefreshDataSource()

		'	Application.DoEvents()

		'	gridView1.BestFitColumns()
		'	gridView1.OptionsSelection.EnableAppearanceFocusedRow = True
		'	gridView1.OptionsSelection.EnableAppearanceHideSelection = False
		'	'gridView1.OptionsView.ColumnAutoWidth = True
		'	gridView1.OptionsView.ShowGroupPanel = False
		'	gridView1.OptionsNavigation.AutoFocusNewRow = False

		'	gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = HorizontalAlignment.Center

		'	gridView1.OptionsBehavior.ReadOnly = True
		'	gridView1.OptionsBehavior.Editable = False

		'	'----------------------------------------------------
		'	iOpen.Enabled = False
		'	iClose.Enabled = True

		'	'---------------------------------------------------------------------------
		'	' Todo: Adjust Grid Columns width not with the buit-in properties
		'	'----------------------------------------------------
		'	' Adjust GridControl Hozrizontal extension
		'	'----------------------------------------------------
		'	'' ''gridControl.ForceInitialize()
		'	'' ''Dim vi As GridViewInfo = TryCast(gridView1.GetViewInfo(), GridViewInfo)
		'	'' ''Dim sizes As New Dictionary(Of GridColumn, Integer)()
		'	'' ''For Each info As GridColumnInfoArgs In vi.ColumnsInfo
		'	'' ''    If info.Column Is Nothing Then
		'	'' ''        Continue For
		'	'' ''    End If
		'	'' ''    sizes.Add(info.Column, info.Bounds.Width)
		'	'' ''Next info
		'	'' ''gridView1.OptionsView.ColumnAutoWidth = False
		'	'' ''For Each c As GridColumn In gridView1.Columns
		'	'' ''    c.Width = sizes(c)
		'	'' ''Next c
		'	'----------------------------------------------------            
		'End If

#End Region
	End Sub

	Private Sub iReOpen_ItemClick(sender As Object, e As ItemClickEventArgs) Handles iReRead.ItemClick

		'	Show wait message
		Call ShowWaitFormOnTheButtomCenter("Reading file", "Please wait ...")

		IsReadFileActive = True
		IsGenerateCRCFileActive = False

		Dim result As String = ""

		Try

			result = Me.ReadFile(Me.inputFileName, Me._crcFileRecordsSplitFields, Me._crcFileRecordsCommaFields)

		Catch ex As Exception
			AlertControl1.Show(Me, "Reading CSV file", ex.Message)
			XtraMessageBox.Show(ex.Message, "Error Reading CRC file", MessageBoxButtons.OK, MessageBoxIcon.Error)
		End Try

		Try

			'	Show wait message
			SplashScreenManager.CloseDefaultWaitForm()

		Catch ex As Exception

		End Try


		If Not String.IsNullOrEmpty(result) Then
			AlertControl1.Show(Me, Application.ProductName, result)
		End If


	End Sub

	Private Sub iClose_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles iClose.ItemClick

		gridView1.Columns.Clear()
		gridControl.DataSource = Nothing
		gridControl.RefreshDataSource()

		_crcFileRecordsSplitFields.Clear()
		_crcFileRecordsCommaFields.Clear()

		TxtGeneratorTool.Text = ""
		TxtFileType.Text = ""
		TxtCreationDate.Text = ""
		TxtDataOriginator.Text = ""
		TxtCompanyName.Text = ""
		TxtSourceFileName.Text = ""
		TxtLastModified.Text = ""

		iClose.Enabled = False
		iReRead.Enabled = False
		iOpen.Enabled = True

		iGenerateCrc.Enabled = False
		iExportCrc.Enabled = False
		iExportCsv.Enabled = False

		_validCrcRecordsCount = 0
		recordsDataTable.Clear()

		siCrcTotalRecords.Caption = "Total Records: " & recordsDataTable.Rows.Count
		siCrcValidRecords.Caption = "Valid CRC Records: " & _validCrcRecordsCount
		siCrcInvalidRecords.Caption = "Invalid CRC Records: " & recordsDataTable.Rows.Count - _validCrcRecordsCount

		crcValidationProgress.EditValue = RepositoryItemProgressBar1.Minimum

		Me.inputFileName = ""

		splitContainerControl.Panel2.Text = "Opened File: "

		IsReadFileActive = False
		IsGenerateCRCFileActive = False
	End Sub

	Private Sub iGenerateCrc_ItemClick(sender As Object, e As ItemClickEventArgs) Handles iGenerateCrc.ItemClick

		If String.IsNullOrEmpty(Me.inputFileName) Then
			AlertControl1.Show(Me, "Generating CRC Result", String.Format("No file selected"))
			XtraMessageBox.Show("No file selected, please select a file first", "Generating CRC Result", MessageBoxButtons.OK, MessageBoxIcon.Error)

			Exit Sub
		End If

		If Not File.Exists(Me.inputFileName) Then
			AlertControl1.Show(Me, "Generating CRC Result", String.Format("File not exist:{0}{1}", vbNewLine, Me.inputFileName))
			XtraMessageBox.Show(String.Format("File not exist:{0}{1}", vbNewLine, Me.inputFileName), "Generating CRC Result", MessageBoxButtons.OK, MessageBoxIcon.Error)
			Exit Sub
		End If


		'	Show wait message
		Call ShowWaitFormOnTheButtomCenter("Generating CRC", "Please wait ...")

		IsReadFileActive = False
		IsGenerateCRCFileActive = True

		'   Read the selected file
		Dim result As String = Me.GenerateCrc(Me.inputFileName, Me._crcFileRecordsSplitFields, Me._crcFileRecordsCommaFields)

		'IsReadFileActive = False
		'IsGenerateCRCFileActive = false

		Try

			'	Show wait message
			SplashScreenManager.CloseDefaultWaitForm()

		Catch ex As Exception

		End Try

		If Not String.IsNullOrEmpty(result) Then
			AlertControl1.Show(Me, "Generating CRC Result", result)
		End If

		'----------------------------------------------------
		iOpen.Enabled = False
		iClose.Enabled = True
		iReRead.Enabled = True

		iGenerateCrc.Enabled = True
		iExportCrc.Enabled = True
		iExportCsv.Enabled = True
	End Sub


	Private Sub iExportCrc_ItemClick(sender As Object, e As ItemClickEventArgs) Handles iExportCrc.ItemClick

		If String.IsNullOrEmpty(Me.inputFileName) Then
			AlertControl1.Show(Me, "Export CRC Result", String.Format("No file selected"))
			XtraMessageBox.Show("No file selected, please select a file first", "Export CRC Result", MessageBoxButtons.OK, MessageBoxIcon.Error)

			Exit Sub
		End If

		'	Export the grid control to CSV file and rename the file to *.CRC

		Try
			Dim fileExtension As String = Path.GetExtension(Me.inputFileName)

			'Dim newCrcFileName As String = String.Format("{0}.crc", Path.Combine(Path.GetDirectoryName(Me.inputFileName), Path.GetFileNameWithoutExtension(Me.inputFileName)))
			'Dim newCrcFileName As String = String.Format("{0}_crc{1}", Path.Combine(Path.GetDirectoryName(Me.inputFileName), Path.GetFileNameWithoutExtension(Me.inputFileName)), fileExtension)
			Dim newCrcFileName As String = String.Format("{0}.crc", Path.Combine(Path.GetDirectoryName(Me.inputFileName), Path.GetFileName(Me.inputFileName)))

			gridControl.ExportToCsv(newCrcFileName, New CsvExportOptions(",", Encoding.UTF8, TextExportMode.Text, False, False))
			'gridControl.ExportToCsv(newCrcFileName)

			Dim diagResult As DialogResult = XtraMessageBox.Show(String.Format("File Exported Successfully to:{0}{0}{1}{0}{0} Do you want to preview it?", vbNewLine, newCrcFileName), "Export CRC file", MessageBoxButtons.YesNo, MessageBoxIcon.Question)

			If diagResult.Equals(DialogResult.Yes) Then
				Dim vWinExplorer As New Process()
				vWinExplorer.StartInfo.FileName = "explorer.exe"

				'	Only select\highlight the file
				vWinExplorer.StartInfo.Arguments = "/select," & newCrcFileName

				'	Open the file using its default program
				'vWinExplorer.StartInfo.Arguments = newCrcFileName
				vWinExplorer.Start()
				vWinExplorer.WaitForExit()
				vWinExplorer.Close()
				vWinExplorer.Dispose()
			End If
		Catch ex As Exception
			AlertControl1.Show(Me, "Export CRC Result", ex.Message)
			XtraMessageBox.Show(ex.Message, "Export CRC Result", MessageBoxButtons.OK, MessageBoxIcon.Error)
		End Try

	End Sub

	Private Sub iExportCsv_ItemClick(sender As Object, e As ItemClickEventArgs) Handles iExportCsv.ItemClick

		If String.IsNullOrEmpty(Me.inputFileName) Then
			AlertControl1.Show(Me, "Export CSV Result", String.Format("No file selected"))
			XtraMessageBox.Show("No file selected, please select a file first", "Export CSV Result", MessageBoxButtons.OK, MessageBoxIcon.Error)

			Exit Sub
		End If

		'	Export the grid control to CSV file and rename the file to *.CRC

		Try
			'dim fileExtension as String = Path.GetExtension(Me.inputFileName)

			'Dim newCrcFileName As String = String.Format("{0}.crc", Path.Combine(Path.GetDirectoryName(Me.inputFileName), Path.GetFileNameWithoutExtension(Me.inputFileName)))
			'Dim newCrcFileName As String = String.Format("{0}_crc{1}", Path.Combine(Path.GetDirectoryName(Me.inputFileName), Path.GetFileNameWithoutExtension(Me.inputFileName)),fileExtension)
			Dim newCrcFileName As String = String.Format("{0}_crc.csv", Path.Combine(Path.GetDirectoryName(Me.inputFileName), Path.GetFileNameWithoutExtension(Me.inputFileName)))

			gridControl.ExportToCsv(newCrcFileName, New CsvExportOptions(",", Encoding.UTF8, TextExportMode.Text, False, False))
			'gridControl.ExportToCsv(newCrcFileName)

			Dim diagResult As DialogResult = XtraMessageBox.Show(String.Format("File Exported Successfully to:{0}{0}{1}{0}{0} Do you want to preview it?", vbNewLine, newCrcFileName), "Export CSV file", MessageBoxButtons.YesNo, MessageBoxIcon.Question)

			If diagResult.Equals(DialogResult.Yes) Then
				Dim vWinExplorer As New Process()
				vWinExplorer.StartInfo.FileName = "explorer.exe"

				'	Only select\highlight the file
				vWinExplorer.StartInfo.Arguments = "/select," & newCrcFileName

				'	Open the file using its default program
				'vWinExplorer.StartInfo.Arguments = newCrcFileName
				vWinExplorer.Start()
				vWinExplorer.WaitForExit()
				vWinExplorer.Close()
				vWinExplorer.Dispose()
			End If
		Catch ex As Exception
			AlertControl1.Show(Me, "Export CSV Result", ex.Message)
			XtraMessageBox.Show(ex.Message, "Export CSV Result", MessageBoxButtons.OK, MessageBoxIcon.Error)
		End Try
	End Sub

	Private Sub gridControl_SizeChanged(ByVal sender As Object, ByVal e As EventArgs) Handles gridControl.SizeChanged
		'  Todo: uncomment these lins to enable adusting the grid column with acc.to the grid width
		'' ''Dim info As GridViewInfo = TryCast(gridView1.GetViewInfo(), GridViewInfo)
		'' ''Dim minWidth As Integer = 0
		'' ''For Each c As GridColumn In gridView1.Columns
		'' ''    minWidth += c.MinWidth
		'' ''Next c

		'' ''gridView1.OptionsView.ColumnAutoWidth = (info.ViewRects.ColumnPanelWidth > minWidth)
	End Sub

	Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


		iClose.Enabled = False
		iReRead.Enabled = False
		iOpen.Enabled = True

		iGenerateCrc.Enabled = False
		iExportCrc.Enabled = False
		iExportCsv.Enabled = False

		Me.Text = String.Format("{0} v{1}", Application.ProductName, Application.ProductVersion)

		siStatus.Caption = "CRC File Details: "
		siCrcTotalRecords.Caption = "Total Records: " & recordsDataTable.Rows.Count
		siCrcValidRecords.Caption = "Valid CRC Records: " & _validCrcRecordsCount
		siCrcInvalidRecords.Caption = "Invalid CRC Records: " & recordsDataTable.Rows.Count - _validCrcRecordsCount

		crcValidationProgress.Width = 150

		AlertControl1.ShowPinButton = False

		'gridView1.OptionsView.ColumnAutoWidth = False
		'gridView1.ScrollStyle = ScrollStyleFlags.LiveHorzScroll Or ScrollStyleFlags.LiveVertScroll
		'gridView1.HorzScrollVisibility = ScrollVisibility.Always


		LoadCustomLogoOnRibbon(My.Resources.UnitedATS_HeaderLogo98x98)

		'BarEditItem2.EditValue = My.Resources.UATS_Logo_New_small  ' SystemIcons.Exclamation.ToBitmap()

		'BarButtonItem1.edit = My.Resources.UATS_Logo_New_small

		'BarEditItem1.EditValue = My.Resources.UATS_Logo_New_small


	End Sub

	Private Sub LoadCustomLogoOnRibbon(Optional ByVal imgObj As Image = Nothing, Optional ByVal imgName As String = "")
		Dim skin As Skin = RibbonSkins.GetSkin(DefaultLookAndFeel1.LookAndFeel)

		Dim elem As SkinElement = skin(RibbonSkins.SkinTabPanel)
		elem.Color.BackColor = Color.White
		elem.Image.Stretch = SkinImageStretch.Stretch

		'elem.Image.SizingMargins.Right = 90  ' 98
		'elem.Image.SizingMargins.Left = 0   '6
		'elem.Image.SizingMargins.Top = 80
		'elem.Image.SizingMargins.Bottom = 6 '7

		elem.Image.SizingMargins.Right = 90  ' 98
		elem.Image.SizingMargins.Left = 10   '6
		elem.Image.SizingMargins.Top = 80
		elem.Image.SizingMargins.Bottom = 6 '7

		elem.Image.Layout = SkinImageLayout.Horizontal

		If Not IsNothing(imgObj) Then
			elem.Image.SetImage(imgObj, Color.Transparent)
		ElseIf Not String.IsNullOrEmpty(imgName) Then
			elem.Image.SetImage(imgName, Color.Transparent)
		Else
			elem.Image.SetImage(My.Resources.UnitedATS_HeaderLogo98x98, Color.Transparent)
		End If
	End Sub

	Private Sub iAbout_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles iAbout.ItemClick
		Dim aboutForm As New FrmAbout

		aboutForm.ShowDialog(Me)
	End Sub

	Private Sub AlertControl1_BeforeFormShow(ByVal sender As Object, ByVal e As DevExpress.XtraBars.Alerter.AlertFormEventArgs) Handles AlertControl1.BeforeFormShow
		e.AlertForm.Location = Me.Location
		e.AlertForm.OpacityLevel = 50
	End Sub

	Private Sub ShowWaitFormOnTheButtomCenter(ByVal caption As String, ByVal description As String)
		SplashScreenManager.ShowDefaultWaitForm(Me, True, True, False, 2, caption, description)
		SplashScreenManager.Default.SplashFormStartPosition = SplashFormStartPosition.Manual
		SplashScreenManager.Default.SplashFormLocation = New Point(splitContainerControl.Left + Me.Left + (Me.Width / 2) - 100, splitContainerControl.Top + Me.Top + Me.Height - 300)
		Application.DoEvents
	End Sub


End Class
