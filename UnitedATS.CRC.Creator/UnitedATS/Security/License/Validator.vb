﻿
Imports System.IO
'Imports DeviceId
'Imports DeviceId.Formatters
'Imports Rhino.Licensing

Namespace UnitedATS.Security.License

	Public Class Validator


		'		Public Property PublicKey() As String
		'		Public Property PrivateKey() As String
		'		Public Property PublicKeyFilePathName() As String
		'		Public Property PrivateKeyFilePathName() As String
		'		Public Property ApplicationGuid() As String
		'		Public Property ApplicationName() As String
		'		Public Property ApplicationType() As String
		'		Public Property AerodromeIdent() As String
		'		Public Property LicenseType() As LicenseType
		'		Public Property LicenseFileName() As String
		'		Public Property StartDate() As Date
		'		Public Property ExpirationDate() As Date
		'		Public Property MachineId() As String

		'		Public Sub New()
		'			Me.PublicKey = ""
		'			Me.PrivateKey = ""
		'			Me.PublicKeyFilePathName = "Public.xml"
		'			Me.PrivateKeyFilePathName = "Private.xml"
		'			Me.ApplicationGuid = New Guid(CType(My.Application.GetType.Assembly.GetCustomAttributes(GetType(Runtime.InteropServices.GuidAttribute), False)(0), Runtime.InteropServices.GuidAttribute).Value).ToString()
		'			Me.ApplicationName = IO.Path.GetFileNameWithoutExtension(Application.ExecutablePath)
		'			Me.LicenseType = LicenseType.Personal
		'			Me.LicenseFileName = Me.ApplicationName & ".lic"
		'			Me.ExpirationDate = Nothing
		'			Me.StartDate = Nothing

		'			Me.ApplicationType = Me.ApplicationName
		'			Me.AerodromeIdent = "ALL"

		'			Me.MachineId = ""
		'		End Sub

		'		''' <summary>
		'		''' Define the new License Generator class
		'		''' </summary>
		'		''' <param name="publicKey">Public key from the project resource manager</param>
		'		''' <param name="applicationGuid">The Application GUID</param>
		'		''' <param name="applicationName">The Application Name like: (UnitedATS.eTOD.Interface)</param>
		'		''' <param name="applicationType">The Application Type like: (eTOD.Interface)</param>
		'		''' <param name="licenseType">The Application License Type like: (Personal, Standrad)</param>
		'		''' <param name="expirationDate">The Application License Expiration Date</param>
		'		''' <param name="aerodromeIdent">The Application License Aerodrome like: (ALL, OERY, OOMS, ...)</param>
		'		Public Sub New(ByVal publicKey As String, ByVal applicationGuid As String, ByVal applicationName As String, ByVal applicationType As String, ByVal licenseType As LicenseType,
		'					   Optional ByVal expirationDate As Date = Nothing, Optional ByVal aerodromeIdent As String = "ALL")

		'			MyClass.New

		'			Me.PublicKey = publicKey
		'			Me.ApplicationGuid = applicationGuid.ToUpper()
		'			Me.ApplicationName = applicationName
		'			Me.LicenseType = licenseType
		'			Me.LicenseFileName = Path.GetFullPath(IO.Path.Combine(My.Computer.FileSystem.SpecialDirectories.AllUsersApplicationData, "..\..\" & applicationName & "\" & "license.lic"))
		'			Me.ExpirationDate = expirationDate

		'			Me.ApplicationType = applicationType
		'			Me.AerodromeIdent = aerodromeIdent.ToUpper()

		'			Me.MachineId = Me.GetMachineId()
		'		End Sub


		'		Public Function ChkAppLicense() As Boolean

		'			If Not File.Exists(Me.LicenseFileName) Then
		'				Throw New Rhino.Licensing.LicenseFileNotFoundException
		'			End If

		'			Try

		'				Dim validator = New LicenseValidator(Me.PublicKey, Me.LicenseFileName)

		'				' Validate License
		'				validator.AssertValidLicense()

		'				With validator
		'					'If .UserId.Equals(New Guid(Me.ApplicationGuid)) AndAlso .ExpirationDate.Subtract(Now).Days >= 0 AndAlso .Name.Equals(Me.ApplicationName) AndAlso .LicenseType.Equals(LicenseType.Personal) AndAlso .LicenseAttributes("MachineID").Equals(Me.GetMachineId) Then
		'					If .UserId.Equals(New Guid(Me.ApplicationGuid)) AndAlso .ExpirationDate.Subtract(Now).Days >= 0 AndAlso .LicenseAttributes("MachineID").Equals(Me.MachineId) Then
		'						Return True
		'					Else
		'						Throw New Exception("Invalid License for this application")
		'					End If
		'				End With

		'			Catch ex As Rhino.Licensing.LicenseNotFoundException
		'				' License not found

		'				Throw New Rhino.Licensing.LicenseNotFoundException
		'			Catch ex As Rhino.Licensing.LicenseFileNotFoundException
		'				' License file is not found

		'				Throw New Rhino.Licensing.LicenseFileNotFoundException
		'			Catch ex As Rhino.Licensing.FloatingLicenseNotAvailableException
		'				' Floating license is not availible

		'				Throw New Rhino.Licensing.FloatingLicenseNotAvailableException
		'			Catch ex As Rhino.Licensing.LicenseExpiredException
		'				' License expired

		'				Throw New Rhino.Licensing.LicenseExpiredException
		'			Catch ex As Exception
		'				' Unknown error

		'				Throw New Exception(ex.Message)
		'			End Try
		'		End Function

		'		Private Function GetMachineId() As String

		'			Dim thisMachineId As New DeviceId.DeviceIdBuilder()

		'			With thisMachineId
		'				.AddComponent(New ApplicationInfoDeviceIdComponent("AerodromeIdent", Me.AerodromeIdent))
		'				.AddComponent(New ApplicationInfoDeviceIdComponent("ApplicationName", Me.ApplicationName))     ' "UnitedATS.eTOD.Interface"
		'				.AddComponent(New ApplicationInfoDeviceIdComponent("ApplicationType", Me.ApplicationType))
		'				.AddComponent(New BiosIdDeviceIdComponent())
		'				.AddComponent(New HardDiskIdDeviceIdComponent())
		'				'.AddMachineName()
		'				'.AddMacAddress()
		'				.AddProcessorId()
		'				.AddMotherboardSerialNumber()

		'				Return .ToString(New Base64DeviceIdFormatter(hashName:="SHA384", urlEncode:=False)).ToUpper() & .ToString(New Base64DeviceIdFormatter(hashName:="SHA512", urlEncode:=False)).ToUpper()
		'			End With
		'		End Function

		'#Region "		License Generator Section - NOT Used Here	"


		'		Private Sub GenerateLicenseFile()
		'			'Try

		'			'	'	Create the required directory beofre proceding
		'			'	Call IO.Directory.CreateDirectory(IO.Path.GetDirectoryName(Me.LicenseFileName))

		'			'Catch ex As Exception
		'			'	Throw New Exception("Erorr Generating License File: " & ex.Message)
		'			'End Try

		'			'Dim licGenerator = New LicenseGenerator(Me.PrivateKey)

		'			'' generate the license
		'			'Dim licenseStr As String = licGenerator.Generate(Me.ApplicationName, New Guid(Me.ApplicationGuid), Me.ExpirationDate, New Dictionary(Of String, String) From {{"MachineID", Me.GetMachineId}}, Me.LicenseType)

		'			'File.WriteAllText(Me.LicenseFileName, licenseStr)
		'		End Sub

		'		Private Sub GeneratePublicKey()
		'			'' Generate The Private & public keys

		'			'Dim cp As New CspParameters()
		'			'cp.KeyContainerName = Me.LicenseFileName

		'			'Dim rsa = New RSACryptoServiceProvider(2048, New CspParameters())
		'			'rsa.PersistKeyInCsp = False

		'			'File.WriteAllText(Me.PublicKeyFilePathName, rsa.ToXmlString(False))
		'			'File.WriteAllText(Me.PrivateKeyFilePathName, rsa.ToXmlString(True))

		'		End Sub


		'#End Region


		'	End Class


		'#Region "		Additional Machine ID License Classes		"


		'	Public Class ApplicationInfoDeviceIdComponent
		'		Implements IDeviceIdComponent

		'		Private ReadOnly m_ApplicationInfo As String '	Application Type ( OLS - BRA - BRAOLS ) , Application Name ( UnitedATS.EMSS, UnitedATS.eTOD.Interface, ...)

		'		''' <summary>
		'		''' Gets the component value.
		'		''' </summary>
		'		''' <returns>
		'		''' The component value.
		'		''' </returns>
		'		Public Function GetValue() As String Implements IDeviceIdComponent.GetValue
		'			Return m_ApplicationInfo
		'		End Function

		'		''' <summary>
		'		''' Gets the name of the component.
		'		''' </summary>
		'		Public Property Name As String Implements IDeviceIdComponent.Name

		'		''' <summary>
		'		''' Application Type ( OLS - BRA - BRAOLS ) 
		'		''' Application Name ( UnitedATS.EMSS, UnitedATS.eTOD.Interface, ...)
		'		''' </summary>
		'		''' <param name="applicationInfoName"></param>
		'		''' <param name="applicationInfoValue"></param>
		'		Public Sub New(ByVal applicationInfoName As String, ByVal applicationInfoValue As String)
		'			Me.Name = applicationInfoName
		'			m_ApplicationInfo = applicationInfoValue.ToUpper()
		'		End Sub
		'	End Class

		'	Public Class BiosIdDeviceIdComponent
		'		Implements IDeviceIdComponent

		'		''' <summary>
		'		''' Gets the component value.
		'		''' </summary>
		'		''' <returns>
		'		''' The component value.
		'		''' </returns>
		'		Public Function GetValue() As String Implements IDeviceIdComponent.GetValue
		'			Return Identifier("Win32_BIOS", "Manufacturer") & Identifier("Win32_BIOS", "SMBIOSBIOSVersion") & Identifier("Win32_BIOS", "IdentificationCode") & Identifier("Win32_BIOS", "SerialNumber") & Identifier("Win32_BIOS", "ReleaseDate") & Identifier("Win32_BIOS", "Version")
		'		End Function

		'		''' <summary>
		'		''' Gets the name of the component.
		'		''' </summary>
		'		Public Property Name As String Implements IDeviceIdComponent.Name

		'		''' <summary>
		'		''' Get BIOS ID
		'		''' </summary>
		'		Public Sub New()
		'			Me.Name = "BiosId"
		'		End Sub

		'		'Return a hardware identifier
		'		Private Shared Function Identifier(ByVal wmiClass As String, ByVal wmiProperty As String) As String
		'			Dim result As String = ""
		'			Dim mc As New System.Management.ManagementClass(wmiClass)
		'			Dim moc As System.Management.ManagementObjectCollection = mc.GetInstances()
		'			For Each mo As System.Management.ManagementObject In moc
		'				'Only get the first one
		'				If result = "" Then
		'					Try
		'						result = CStr(mo(wmiProperty))
		'						Exit For
		'					Catch
		'					End Try
		'				End If
		'			Next mo
		'			Return result
		'		End Function

		'	End Class

		'	Public Class HardDiskIdDeviceIdComponent
		'		Implements IDeviceIdComponent

		'		''' <summary>
		'		''' Gets the component value.
		'		''' </summary>
		'		''' <returns>
		'		''' The component value.
		'		''' </returns>
		'		Public Function GetValue() As String Implements IDeviceIdComponent.GetValue
		'			Return Identifier("Win32_DiskDrive", "Model") & Identifier("Win32_DiskDrive", "Manufacturer") & Identifier("Win32_DiskDrive", "Signature") & Identifier("Win32_DiskDrive", "TotalHeads")
		'		End Function

		'		''' <summary>
		'		''' Gets the name of the component.
		'		''' </summary>
		'		Public Property Name As String Implements IDeviceIdComponent.Name

		'		''' <summary>
		'		''' Get BIOS ID
		'		''' </summary>
		'		Public Sub New()
		'			Me.Name = "HardDiskId"
		'		End Sub

		'		'Return a hardware identifier
		'		Private Shared Function Identifier(ByVal wmiClass As String, ByVal wmiProperty As String) As String
		'			Dim result As String = ""
		'			Dim mc As New System.Management.ManagementClass(wmiClass)
		'			Dim moc As System.Management.ManagementObjectCollection = mc.GetInstances()
		'			For Each mo As System.Management.ManagementObject In moc
		'				'Only get the first one
		'				If result = "" Then
		'					Try
		'						result = CStr(mo(wmiProperty))
		'						Exit For
		'					Catch
		'					End Try
		'				End If
		'			Next mo
		'			Return result
		'		End Function

	End Class


	'#End Region
End Namespace